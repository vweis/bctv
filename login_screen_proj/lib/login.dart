import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: ListView(
              padding: EdgeInsets.symmetric(horizontal: 24.0),
              children: [
                SizedBox(height: 80.0), // used for space
                Column(
                  children:<Widget>[
                    Image.asset('assets/login_icon.png'),
                    SizedBox(height: 20.0),
                    Text('Login')
                  ]
                //TODO: Part 10 of https://codeburst.io/make-a-material-design-login-page-with-flutter-the-basics-99d3acd80b18
                ),
                SizedBox(height: 120.0),
                TextField(
                  decoration: InputDecoration(
                    labelText: 'username',
                    filled: true
                  )
                ),
                SizedBox(height: 12.0),
                TextField(
                    decoration: InputDecoration(
                        labelText: 'password',
                        filled: true
                    ),
                  obscureText: true,
                ),
                ButtonBar(
                  children: <Widget>[
                    FlatButton(
                      child: Text('Cancel'),
                      onPressed: (){

                      },
                    ),
                    RaisedButton(
                      child: Text('NEXT'),
                      onPressed: (){

                      },
                    )
                  ],
                )
              ]
          )
      )
    );
  }
}



class CircleImage extends StatelessWidget {
  final String renderUrl;

  CircleImage(this.renderUrl);

  @override
  Widget build(BuildContext context) {
    return new Container(
      width: 100.0,
      height: 100.0,
      decoration: new BoxDecoration(
        shape: BoxShape.circle,
        image: new DecorationImage(
          fit: BoxFit.cover,
          image: new NetworkImage(renderUrl ?? ''),
        ),
      ),
    );
  }
}