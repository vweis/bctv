import 'package:flutter/material.dart';
import 'login.dart';
import 'front.dart';
import 'primary.dart';
//TODO: https://startflutter.com/ check this shit out
//TODO: https://www.flaticon.com/ check these icons out
//TODO: Routing with flutter. Fun right?
// https://medium.com/flutter-community/flutter-push-pop-push-1bb718b13c31
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      routes: <String, WidgetBuilder> {
        '/home': (BuildContext context) => new FrontPage(),
        '/login' : (BuildContext context) => new LoginPage(),
        '/primary' : (BuildContext context) => new MapPage(),
        //'/screen4' : (BuildContext context) => new Screen4()
      },
      home: FrontPage()//MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

