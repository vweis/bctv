import 'package:flutter/material.dart';
import 'primary.dart';

class FrontPage extends StatefulWidget {
  FrontPage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _FrontPageState createState() => _FrontPageState();
}

class _FrontPageState extends State<FrontPage> {
  bool isloggedIn = true; //true for now

  void _loginButtonPressed() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen
    });
  }



  @override
  Widget build(BuildContext context) {
    Widget childView;
    switch (isloggedIn) {
      case true:
        /*Navigator.push(
            context,
            MaterialPageRoute(builder: (BuildContext context) => MapPage())
        );*/
        Navigator.of(context).pushNamed('/primary');
        print("Navigator pushed /primary route.");
        break;//Add app screen;
            case false: return LoginPrompt();
      default: return LoginPrompt();
    }

    return Scaffold(
        body: SafeArea(
            child: LoginPrompt()
        )
    );
  }
}

class LoginPrompt extends StatefulWidget {
  @override
  LoginPromptView createState() => LoginPromptView();
}

abstract class LoginPromptState extends State<LoginPrompt> {
  @protected String inputUsername;
  @protected String inputPassword;

  void loginRequest() {
    //TODO: Implement asynchronous login request and update view accordingly
  }
}

class LoginPromptView extends LoginPromptState {
  @override build(BuildContext context) {
    return ListView(
        padding: EdgeInsets.symmetric(horizontal: 24.0),
        children: [
          SizedBox(height: 80.0), // used for space
          Column(
              children:<Widget>[
                Image.asset('assets/login_icon.png'),
                SizedBox(height: 20.0)
              ]
            //TODO: https://codeburst.io/make-a-material-design-login-page-with-flutter-the-basics-99d3acd80b18
          ),
          SizedBox(height: 120.0),
          Text('Login'),
          TextField(
              decoration: InputDecoration(
                  labelText: 'username',
                  filled: true
              )
          ),
          SizedBox(height: 12.0),
          TextField(
            decoration: InputDecoration(
                labelText: 'password',
                filled: true
            ),
            obscureText: true,
          ),
          ButtonBar(
            children: <Widget>[
              FlatButton(
                child: Text('Cancel'),
                onPressed: (){

                },
              ),
              RaisedButton(
                child: Text('NEXT'),
                onPressed: (){
                  //loginRequest();
                  //TODO: Implement Login later, replaced with Route push for now
                  Navigator.of(context).pushNamed('/primary');
                },
              )
            ],
          )
        ]
    );

  }
}