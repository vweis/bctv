//
//  ViewController.swift
//  CSCampUs
//
//  Created by student on 3/14/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class MainViewController: UIViewController, CLLocationManagerDelegate {
    //loaded model elements
    var currentUser: User!
    var events: [Event] = []
    var users: [User] = []
    
    //The Map View needs to have displayed pins for matched events, and have those pins be selectable to transition to Event matching.
    @IBOutlet weak var mapView: MKMapView!
    
    
    var locationManager: CLLocationManager!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Returns sample array of Events into the local events variable
        self.events = Event.loadSampleEvents()
        
        let samplePreferredLocation = CLCircularRegion(center: CLLocationCoordinate2D(latitude: CLLocationDegrees(33.4242), longitude: CLLocationDegrees(111.9281)), radius: CLLocationDistance(1604), identifier: "UserPreferredRegion")
        
        
        
        self.users = [User(username: "Jane Dohn", pwd: "password", interests: [Interests.BoardGames, Interests.Theatre, Interests.VideoGames], matchedEvents: [], preferredLocation: samplePreferredLocation),
                      User(username: "Jack Dane", pwd: "password",  interests: [Interests.Theatre, Interests.VideoGames], matchedEvents: [], preferredLocation: samplePreferredLocation),
        User(username: "John Doe", pwd: "password", interests: [Interests.BoardGames, Interests.Theatre, Interests.VideoGames], matchedEvents: [], preferredLocation: samplePreferredLocation)]
        
        
        //This loads sample User data into the main screen.
        login(username: "John Doe", pwd: "password")
        
    }
    
    //______________________________________________________________
    
    func setupLocationManager() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
    }
    
    
    
    
    
    func login(username: String, pwd: String) {
        for user in users {
            if username == user.username {
                if pwd == user.password {
                    user.login()
                    self.currentUser = user
                    return
                } else {
                    print("Wrong password.")
                }
            } else {
                print("No matched username.")
            }
        }
    }
    
    func matchUserWithEvents() {
        if let user = currentUser {
            for userInterest in user.interests {
                user.matchedEvents = events.filter { (event) -> Bool in
                    for eventInterest in event.associatedInterests {
                        if userInterest == eventInterest {
                            if user.preferredLocation.contains(event.location.coordinate) {
                                return true
                            }
                            
                        }
                    }
                    return false
                }
            }
        }
        print(currentUser.matchedEvents)
    }
    
    
    
    
    
    // __________End Model Manipulation______________
    
    //Display/View Manipulation down here
    
    
    func centerMapAtCoordinate(coordinate: Coordinate) {
        //guard let spot = coordinate else {return}
        
        let center = CLLocationCoordinate2D(coordinate)
        
        
        
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        mapView.setRegion(region, animated: true)
        // TODO: Finish this function.
        
    }
    
    
    func updateMatchedMapPins() {
        print("User matched events: \(currentUser.matchedEvents)")
        for event in currentUser.matchedEvents {
            
            self.mapView.removeAnnotations(self.mapView.annotations)
            
            
            let pin = MKPointAnnotation()
            pin.coordinate = event.location.coordinate
            pin.title = event.name
            self.mapView.addAnnotation(pin)
            
            mapView.addAnnotation(pin)
        }
    }
    
    //___________End Display/View Manipulation_________________
    
    //Button/Interface Handling down here
    
    
    @IBAction func matchButtonPressed(_ sender: UIBarButtonItem) {
        matchUserWithEvents()
        updateMatchedMapPins()
    }
    
    
    
    
    
    
    
    
    
}

