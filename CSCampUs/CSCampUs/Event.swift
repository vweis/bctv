//
//  Event.swift
//  CSCampUs
//
//  Created by student on 3/14/19.
//  Copyright © 2019 student. All rights reserved.
//


import CoreLocation
import UIKit

struct Event {
    var name: String
    var location: CLLocation //TODO: Is this the right type?
    var image: UIImage?
    
    var isPaid: Bool
    var schedule: DateInterval
    
    var associatedInterests: [Interests]
    
    init(name: String, location: String, isPaid: Bool, schedule: DateInterval, interestCategories: [Interests]) {
        self.name = name
        
        //Location initializer
        var locStored: CLLocation = CLLocation(latitude: 33.4242, longitude: 111.9281)
        CLGeocoder().geocodeAddressString(location, completionHandler: {(placemarks, error) in
            guard let place = placemarks?.first! else {
                // ? no placemark
                return
            }
            if let foundLocation = place.location {
                locStored = foundLocation
            } else {
                
            }
        })
        self.location = locStored
        
        self.isPaid = isPaid
        
        //TODO: Replace init part with start and end time and convert to interval
        self.schedule = schedule
        
        self.associatedInterests = interestCategories
        
        self.image = nil
        
    }
    
    
    
    
    static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .short
        return formatter
    }()
    

    
    static func loadSampleEvents() -> [Event] {
        let schedule1 = DateInterval(start: Date(timeIntervalSince1970: 1553454000000), duration: TimeInterval(60*60))
        let event1 = Event(name: "CSGO Tournament", location: "ASU Brickyard", isPaid: true, schedule: schedule1, interestCategories: [Interests.VideoGames])

        
        return [event1]
    }
}
