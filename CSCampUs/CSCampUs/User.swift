//
//  User.swift
//  CSCampUs
//
//  Created by student on 3/14/19.
//  Copyright © 2019 student. All rights reserved.
//


import CoreLocation
import UIKit

//This struct represents any user, and can be used for other users that are matched with the current app user.
class User {
    var username: String
    var avatar: UIImage?
    var interests: [Interests]
    var matchedEvents: [Event]
    var isLoggedIn: Bool
    var password: String
    var preferredLocation: CLCircularRegion
    
    init(username: String, pwd: String, interests: [Interests], matchedEvents: [Event], preferredLocation: CLCircularRegion) {
        self.username = username
        self.avatar = nil
        self.interests = interests
        self.matchedEvents = matchedEvents
        self.isLoggedIn = false
        self.password = pwd
        self.preferredLocation = preferredLocation
    }
    
    func login() {
        self.isLoggedIn = true
    }
}


