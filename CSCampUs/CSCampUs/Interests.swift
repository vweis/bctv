//
//  Interests.swift
//  CSCampUs
//
//  Created by student on 3/14/19.
//  Copyright © 2019 student. All rights reserved.
//


enum Interests {
    case VideoGames, BoardGames, Gym, Food, Movies, Pedestrian, IndoorAthletic, OutdoorAtheletic, Concert, Theatre, Dancing, Shopping, Miscellaneous
}
