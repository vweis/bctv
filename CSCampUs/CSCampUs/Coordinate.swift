//
//  Coordinate.swift
//  ToDoList
//
//  Created by Loren Olson on 2/27/19.
//  Copyright Â© 2019 ASU. All rights reserved.
//

import UIKit
import CoreLocation


struct Coordinate: Codable {
    let latitude: Double
    let longitude: Double
}


extension Coordinate {
    init(_ coordinate: CLLocationCoordinate2D) {
        self.init(latitude: coordinate.latitude, longitude: coordinate.longitude)
    }
}

extension CLLocationCoordinate2D {
    init(_ coordinate: Coordinate) {
        self.init(latitude: coordinate.latitude, longitude: coordinate.longitude)
    }
}

