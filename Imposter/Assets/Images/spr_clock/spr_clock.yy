{
    "id": "9608d20e-341e-4a4f-b770-a6aa42a9d56e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_clock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db0cd348-9cc4-4080-a88b-9039449c2ef5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9608d20e-341e-4a4f-b770-a6aa42a9d56e",
            "compositeImage": {
                "id": "42a9f5ec-5e3b-4583-9d14-bb783a36dc4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db0cd348-9cc4-4080-a88b-9039449c2ef5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b473316-0ec8-496b-ba57-0b03f4f161f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db0cd348-9cc4-4080-a88b-9039449c2ef5",
                    "LayerId": "1308e1fd-f9c3-4ea0-a575-568b2a85d5b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "1308e1fd-f9c3-4ea0-a575-568b2a85d5b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9608d20e-341e-4a4f-b770-a6aa42a9d56e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 0,
    "yorig": 0
}