{
    "id": "fdcc11f6-b1c6-4418-8c3d-408fe2093330",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Countertop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "84082812-c033-4be6-8377-66de8f8534b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcc11f6-b1c6-4418-8c3d-408fe2093330",
            "compositeImage": {
                "id": "3e2f4a79-3f0e-4d50-b9f4-5255acca1339",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84082812-c033-4be6-8377-66de8f8534b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbea650e-5b23-44ae-b376-a5baca51d66d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84082812-c033-4be6-8377-66de8f8534b6",
                    "LayerId": "ff6e453f-5175-4a19-a90a-993aae95ae45"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "ff6e453f-5175-4a19-a90a-993aae95ae45",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fdcc11f6-b1c6-4418-8c3d-408fe2093330",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}