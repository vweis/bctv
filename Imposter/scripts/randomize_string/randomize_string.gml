///@description randomize_string
///@arg stringToRandomize

var argString = argument0

var result = "";
//For all the characters in the string...
var i=1;
for (i=1;i<=string_length(argString);i++) {
	
	//if (!string_char_at(argString, i)) {continue;}
	var c = string_char_at(argString, i);
	
	switch (global.deformation) {
	case 0:
	return argString;
	break;
	
	case 1:
	show_debug_message("deformation is 1.")
	if ((i % 4) != 0) {show_debug_message("continuing");result += c;continue;}
	break;
	
	case 2:
	show_debug_message("deformation is 2.")
	if ((i % 2) != 0) {show_debug_message("continuing");result += c;continue;}
	break;
	
	case 3:
	default:
	show_debug_message("deformation is 3 or above.")
	break;
	}
	
	
	
	switch (c) {
		case " ":
		case ".":
		case "!":
			result += c
			show_debug_message("continuing")
			break
		default:
			break
	}
	
	var charString = string(c)
	var UTFCharVal = ord(charString)
	var debugCharValString = "UTFCharVal:" + string(UTFCharVal) + ""
	show_debug_message(debugCharValString)
	
	if UTFCharVal >= 0x0041 && UTFCharVal <= 0x007A {
		var newSeed = round(random_range(0x00A1, 0x00FF));
		var debugSeedString = "newSeed:" + string(newSeed) + ""
		show_debug_message(debugSeedString)
		
		var myString = chr(newSeed)
		var debugString = "myString:" + string(myString) + ""
		show_debug_message(debugString)
		
		result += myString
	}
	
}
show_debug_message(result)
return result