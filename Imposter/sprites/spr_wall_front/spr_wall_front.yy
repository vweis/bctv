{
    "id": "c926c3cf-acaa-4143-9e7c-c8e4b3abad2c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 18,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a1f5a05c-9c62-4cc9-9a3a-807ed628f5a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c926c3cf-acaa-4143-9e7c-c8e4b3abad2c",
            "compositeImage": {
                "id": "59f03220-fb93-4e59-b470-1d1558a7da29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1f5a05c-9c62-4cc9-9a3a-807ed628f5a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34743d73-eaa5-4904-b963-7c86d4e3d0c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1f5a05c-9c62-4cc9-9a3a-807ed628f5a2",
                    "LayerId": "3f11b30d-08d2-4eeb-9cad-e3b44c376019"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "3f11b30d-08d2-4eeb-9cad-e3b44c376019",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c926c3cf-acaa-4143-9e7c-c8e4b3abad2c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 32
}