{
    "id": "d33b966c-0cb0-4707-a6a4-cc5cf105520d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cubicle_musical",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 17,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ca6feec0-e54f-4c04-846a-3510c859c511",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d33b966c-0cb0-4707-a6a4-cc5cf105520d",
            "compositeImage": {
                "id": "d06c0199-020c-44cb-9c85-f7ea905b51bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca6feec0-e54f-4c04-846a-3510c859c511",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4ae25db-1a9c-40f9-9c5c-3e957c324ecb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca6feec0-e54f-4c04-846a-3510c859c511",
                    "LayerId": "31924254-cb57-443d-8f94-e9c41452f946"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "31924254-cb57-443d-8f94-e9c41452f946",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d33b966c-0cb0-4707-a6a4-cc5cf105520d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 14
}