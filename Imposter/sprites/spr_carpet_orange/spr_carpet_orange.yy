{
    "id": "fce234bd-0c86-4892-b043-01133aff2eb2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_carpet_orange",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "afb70310-aea2-4d3b-8175-3bf545ac67f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fce234bd-0c86-4892-b043-01133aff2eb2",
            "compositeImage": {
                "id": "32649ea4-ce57-4f16-bc79-b79c55164a0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afb70310-aea2-4d3b-8175-3bf545ac67f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "156a1816-aef7-433e-816a-6996cecdbe6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afb70310-aea2-4d3b-8175-3bf545ac67f2",
                    "LayerId": "238a61b1-aa82-4ef7-b79d-62e6aa0c2a53"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "238a61b1-aa82-4ef7-b79d-62e6aa0c2a53",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fce234bd-0c86-4892-b043-01133aff2eb2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}