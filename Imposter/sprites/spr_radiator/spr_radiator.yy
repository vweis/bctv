{
    "id": "7d111430-2381-4f3e-a16f-24758970fe52",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_radiator",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1dedc7b3-e211-4632-8f72-63ef66e654e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d111430-2381-4f3e-a16f-24758970fe52",
            "compositeImage": {
                "id": "b01671fc-aa81-4101-bce7-bef52d538630",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dedc7b3-e211-4632-8f72-63ef66e654e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d32a45ac-25c4-4f53-8365-9aab81e3d7d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dedc7b3-e211-4632-8f72-63ef66e654e1",
                    "LayerId": "e32cbfab-fac6-42c8-a343-520e9d49f985"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "e32cbfab-fac6-42c8-a343-520e9d49f985",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d111430-2381-4f3e-a16f-24758970fe52",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 11,
    "yorig": 21
}