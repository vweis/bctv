{
    "id": "9cf799d7-b493-4a0f-aac1-4ff50e594288",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_idle_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 20,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "52c1ed34-aa6d-48c7-b124-5e6fee45db96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cf799d7-b493-4a0f-aac1-4ff50e594288",
            "compositeImage": {
                "id": "b713ef41-c7f0-4f56-9514-ea39e242ef14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52c1ed34-aa6d-48c7-b124-5e6fee45db96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbbd6e84-fdb7-4a88-b48e-8b61b2f868a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52c1ed34-aa6d-48c7-b124-5e6fee45db96",
                    "LayerId": "8b3b49e4-50bc-4e5e-a0e4-4db97c9361ee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "8b3b49e4-50bc-4e5e-a0e4-4db97c9361ee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9cf799d7-b493-4a0f-aac1-4ff50e594288",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 12
}