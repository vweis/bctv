{
    "id": "6c6c82fc-0f08-40c4-89ba-d8f076ee20bc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_idle_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 22,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "91ad75a6-dac6-418a-979c-7543798bdc34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c6c82fc-0f08-40c4-89ba-d8f076ee20bc",
            "compositeImage": {
                "id": "6ab242e0-538b-4a93-b42a-777662ee0829",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91ad75a6-dac6-418a-979c-7543798bdc34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b601cfe-6fd3-4873-947f-b9cd581f43c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91ad75a6-dac6-418a-979c-7543798bdc34",
                    "LayerId": "8ddec627-d58c-4a6d-81b4-55287743aa48"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "8ddec627-d58c-4a6d-81b4-55287743aa48",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c6c82fc-0f08-40c4-89ba-d8f076ee20bc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 12
}