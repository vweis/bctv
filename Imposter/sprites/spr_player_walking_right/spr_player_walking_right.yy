{
    "id": "2728263f-6d3a-41bc-9023-bc47c17b5791",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_walking_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b3e177a-0066-47fa-8c0e-a4b8d1d98979",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2728263f-6d3a-41bc-9023-bc47c17b5791",
            "compositeImage": {
                "id": "e8031984-1e20-4e60-8a01-3df82b07443d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b3e177a-0066-47fa-8c0e-a4b8d1d98979",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec2a530c-480b-4c92-a3d5-2beaf60a5101",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b3e177a-0066-47fa-8c0e-a4b8d1d98979",
                    "LayerId": "bae914c6-40c4-422f-9768-e491a455c998"
                }
            ]
        },
        {
            "id": "5564a8b9-79a2-4aac-b464-0e5f4947ada3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2728263f-6d3a-41bc-9023-bc47c17b5791",
            "compositeImage": {
                "id": "4ff1a768-1b6c-4de1-949c-6f6ecc428eef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5564a8b9-79a2-4aac-b464-0e5f4947ada3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0833958c-77e9-4afb-a341-07e34287eccd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5564a8b9-79a2-4aac-b464-0e5f4947ada3",
                    "LayerId": "bae914c6-40c4-422f-9768-e491a455c998"
                }
            ]
        },
        {
            "id": "1c9bb49c-8b2e-4901-a17b-4411d30af94e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2728263f-6d3a-41bc-9023-bc47c17b5791",
            "compositeImage": {
                "id": "2c3ddd46-7631-4a74-97de-4ef54bb4e21c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c9bb49c-8b2e-4901-a17b-4411d30af94e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a783d6aa-85e0-4d6f-a9e7-67bef340d975",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c9bb49c-8b2e-4901-a17b-4411d30af94e",
                    "LayerId": "bae914c6-40c4-422f-9768-e491a455c998"
                }
            ]
        },
        {
            "id": "5bccfd75-e39e-4f5f-aaee-65faf8e5228c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2728263f-6d3a-41bc-9023-bc47c17b5791",
            "compositeImage": {
                "id": "93c4ba9e-3aa0-48de-902d-c50d6de5c2a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bccfd75-e39e-4f5f-aaee-65faf8e5228c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "866fef55-7a7f-4c93-bf10-0e5b33bc6b20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bccfd75-e39e-4f5f-aaee-65faf8e5228c",
                    "LayerId": "bae914c6-40c4-422f-9768-e491a455c998"
                }
            ]
        },
        {
            "id": "4ba546e8-8cfb-411a-bd95-9e889709ad57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2728263f-6d3a-41bc-9023-bc47c17b5791",
            "compositeImage": {
                "id": "8a4dce43-2f98-40bc-adbe-4cfe9fa1634c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ba546e8-8cfb-411a-bd95-9e889709ad57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6013c801-ce79-487b-ad51-9c29c64d3312",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ba546e8-8cfb-411a-bd95-9e889709ad57",
                    "LayerId": "bae914c6-40c4-422f-9768-e491a455c998"
                }
            ]
        },
        {
            "id": "d49f3317-01c3-4606-8bf4-387e5d1d67bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2728263f-6d3a-41bc-9023-bc47c17b5791",
            "compositeImage": {
                "id": "205ff661-2916-4c37-8bce-9ea3d4835719",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d49f3317-01c3-4606-8bf4-387e5d1d67bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e5b9e44-c4c9-4261-8d18-d7321392a56c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d49f3317-01c3-4606-8bf4-387e5d1d67bc",
                    "LayerId": "bae914c6-40c4-422f-9768-e491a455c998"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "bae914c6-40c4-422f-9768-e491a455c998",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2728263f-6d3a-41bc-9023-bc47c17b5791",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 12
}