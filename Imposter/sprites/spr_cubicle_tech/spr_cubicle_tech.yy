{
    "id": "c302d173-c231-4c44-843d-5f33ccb40488",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cubicle_tech",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 17,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a9ee024e-1905-4659-988a-3e0387e2610f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c302d173-c231-4c44-843d-5f33ccb40488",
            "compositeImage": {
                "id": "4d5be748-c99c-47f2-8c47-962e93d62bd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9ee024e-1905-4659-988a-3e0387e2610f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "deb7e141-ce72-4008-b19b-f38e49cfa9af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9ee024e-1905-4659-988a-3e0387e2610f",
                    "LayerId": "fa27ebd4-ba9e-4a67-9ba4-a9cc7fa7bfb4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "fa27ebd4-ba9e-4a67-9ba4-a9cc7fa7bfb4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c302d173-c231-4c44-843d-5f33ccb40488",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 14
}