{
    "id": "ca425de0-d0bc-4f61-816a-69a77014057f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_chair_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9d56e1e-26be-46f6-90ec-ec7e605d9b78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca425de0-d0bc-4f61-816a-69a77014057f",
            "compositeImage": {
                "id": "49834587-81a1-43c2-b821-e73ab52fece9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9d56e1e-26be-46f6-90ec-ec7e605d9b78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87b7254c-5fee-4a5a-b5cf-8cd0775e2634",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9d56e1e-26be-46f6-90ec-ec7e605d9b78",
                    "LayerId": "79a814bd-f0e0-4a47-8da3-3f917708a440"
                }
            ]
        },
        {
            "id": "200b0a6b-9280-40a9-9804-1a0a28352e25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca425de0-d0bc-4f61-816a-69a77014057f",
            "compositeImage": {
                "id": "f506ae98-3c39-4531-bfec-507f5d4944a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "200b0a6b-9280-40a9-9804-1a0a28352e25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c16c8937-2735-41de-9b92-73c703c1f078",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "200b0a6b-9280-40a9-9804-1a0a28352e25",
                    "LayerId": "79a814bd-f0e0-4a47-8da3-3f917708a440"
                }
            ]
        },
        {
            "id": "7599b401-1b99-481c-b07a-23e319f7fe4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca425de0-d0bc-4f61-816a-69a77014057f",
            "compositeImage": {
                "id": "c8e262f1-b6fa-4977-9797-936f2479908c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7599b401-1b99-481c-b07a-23e319f7fe4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af5a568f-6a2e-47cb-927e-aac132b44e55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7599b401-1b99-481c-b07a-23e319f7fe4e",
                    "LayerId": "79a814bd-f0e0-4a47-8da3-3f917708a440"
                }
            ]
        },
        {
            "id": "6a8ce9f0-9e87-4235-9e6b-c306ed45e0c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca425de0-d0bc-4f61-816a-69a77014057f",
            "compositeImage": {
                "id": "0ec1fdf0-ccfc-4567-916b-894f6f3c13b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a8ce9f0-9e87-4235-9e6b-c306ed45e0c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ceedc89f-fb48-4619-9ea9-332d33f5f46a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a8ce9f0-9e87-4235-9e6b-c306ed45e0c8",
                    "LayerId": "79a814bd-f0e0-4a47-8da3-3f917708a440"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "79a814bd-f0e0-4a47-8da3-3f917708a440",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca425de0-d0bc-4f61-816a-69a77014057f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 11
}