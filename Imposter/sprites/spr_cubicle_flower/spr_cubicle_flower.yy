{
    "id": "5e5ca886-2ea8-473b-921a-ba45ece98854",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cubicle_flower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 17,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5ac42f17-7aef-49ba-8707-2ced3786c307",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e5ca886-2ea8-473b-921a-ba45ece98854",
            "compositeImage": {
                "id": "81fbcfbc-17c8-47fa-9112-e522e347cb00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ac42f17-7aef-49ba-8707-2ced3786c307",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3222ad77-6cf8-4351-8477-73939335b5c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ac42f17-7aef-49ba-8707-2ced3786c307",
                    "LayerId": "3d71c5aa-266f-4d3d-9c01-dd19b04acc07"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "3d71c5aa-266f-4d3d-9c01-dd19b04acc07",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e5ca886-2ea8-473b-921a-ba45ece98854",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 14
}