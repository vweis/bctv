{
    "id": "618827f1-548c-4782-8b16-c518c27a0f5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cubicle_steamPunk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 17,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6937f94-bfae-4f94-ab62-25e0c3aad658",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "618827f1-548c-4782-8b16-c518c27a0f5c",
            "compositeImage": {
                "id": "17676069-b151-469c-bb6b-47472f32df8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6937f94-bfae-4f94-ab62-25e0c3aad658",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc809b3f-9e48-42f3-9375-3efec529da18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6937f94-bfae-4f94-ab62-25e0c3aad658",
                    "LayerId": "35d36ffb-43df-44d9-94ce-c4c3f39919e4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "35d36ffb-43df-44d9-94ce-c4c3f39919e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "618827f1-548c-4782-8b16-c518c27a0f5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 14
}