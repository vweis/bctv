{
    "id": "f81d79ab-9bc3-4bc6-98ec-2cde3e65fc4b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_walking_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cbf69d63-8c73-4583-a3c1-78c3f703fd00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d79ab-9bc3-4bc6-98ec-2cde3e65fc4b",
            "compositeImage": {
                "id": "02eba4ab-f471-4c46-871f-91b2e672b55f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbf69d63-8c73-4583-a3c1-78c3f703fd00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15a09e44-3ae0-462b-88ab-8397b13d9950",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbf69d63-8c73-4583-a3c1-78c3f703fd00",
                    "LayerId": "08c84ee0-fefe-48ab-9abf-abf7d774b782"
                }
            ]
        },
        {
            "id": "98714ee4-b990-4c28-a4fa-1cd99526433a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d79ab-9bc3-4bc6-98ec-2cde3e65fc4b",
            "compositeImage": {
                "id": "4eec1d6b-c370-492d-ad1f-0b61b9f947ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98714ee4-b990-4c28-a4fa-1cd99526433a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ea55d31-c385-43aa-830b-11ec8f62b17d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98714ee4-b990-4c28-a4fa-1cd99526433a",
                    "LayerId": "08c84ee0-fefe-48ab-9abf-abf7d774b782"
                }
            ]
        },
        {
            "id": "e4e245c2-08b6-4e2a-82c7-7c39ae677faf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d79ab-9bc3-4bc6-98ec-2cde3e65fc4b",
            "compositeImage": {
                "id": "59d59f69-a422-4a9b-bc25-94e26451952d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4e245c2-08b6-4e2a-82c7-7c39ae677faf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59b988a5-bde2-49eb-a495-878202f8fc4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4e245c2-08b6-4e2a-82c7-7c39ae677faf",
                    "LayerId": "08c84ee0-fefe-48ab-9abf-abf7d774b782"
                }
            ]
        },
        {
            "id": "f7068a26-536c-4ada-8362-9111aa59c4d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d79ab-9bc3-4bc6-98ec-2cde3e65fc4b",
            "compositeImage": {
                "id": "57bbe1d3-a564-4c87-a651-4359a7bf9130",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7068a26-536c-4ada-8362-9111aa59c4d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d84710bc-d951-4a41-9766-abb9cf8aa8d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7068a26-536c-4ada-8362-9111aa59c4d3",
                    "LayerId": "08c84ee0-fefe-48ab-9abf-abf7d774b782"
                }
            ]
        },
        {
            "id": "5e41fe93-1bc3-44a3-9f87-ee9ffb354f2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d79ab-9bc3-4bc6-98ec-2cde3e65fc4b",
            "compositeImage": {
                "id": "efffabb2-1782-4bf6-98d1-9453aa7ff1b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e41fe93-1bc3-44a3-9f87-ee9ffb354f2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "018cee7c-a7a5-4ed8-96d2-dc9272f199af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e41fe93-1bc3-44a3-9f87-ee9ffb354f2e",
                    "LayerId": "08c84ee0-fefe-48ab-9abf-abf7d774b782"
                }
            ]
        },
        {
            "id": "bb990550-cca3-4ebd-852c-dd1d477a11b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d79ab-9bc3-4bc6-98ec-2cde3e65fc4b",
            "compositeImage": {
                "id": "040400a9-4af0-4a85-88c8-cb4a5a83faa7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb990550-cca3-4ebd-852c-dd1d477a11b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c41540e8-52a5-40bd-b403-a670f6daf47c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb990550-cca3-4ebd-852c-dd1d477a11b2",
                    "LayerId": "08c84ee0-fefe-48ab-9abf-abf7d774b782"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "08c84ee0-fefe-48ab-9abf-abf7d774b782",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f81d79ab-9bc3-4bc6-98ec-2cde3e65fc4b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 12
}