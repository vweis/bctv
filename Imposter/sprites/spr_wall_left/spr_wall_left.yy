{
    "id": "94888290-1b3e-4724-815e-f0227f7a1ed3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "36a3c191-f6a8-40f1-9527-22ce56d5be1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94888290-1b3e-4724-815e-f0227f7a1ed3",
            "compositeImage": {
                "id": "3a3ba925-456e-4eaf-8ce1-6b8657d7f098",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36a3c191-f6a8-40f1-9527-22ce56d5be1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e038214-758a-44cc-a3d6-f4a1122e1a98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36a3c191-f6a8-40f1-9527-22ce56d5be1c",
                    "LayerId": "68024eb4-3fb6-4851-86dd-3246b4f1052c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "68024eb4-3fb6-4851-86dd-3246b4f1052c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94888290-1b3e-4724-815e-f0227f7a1ed3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 2,
    "yorig": 31
}