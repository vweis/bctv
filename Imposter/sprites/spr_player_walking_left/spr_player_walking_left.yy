{
    "id": "250dab8e-6580-4309-acfe-62756123cd76",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_walking_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fb477801-044d-438c-af06-290c7426d248",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "250dab8e-6580-4309-acfe-62756123cd76",
            "compositeImage": {
                "id": "0b2b662d-4271-4435-81e2-b7f2c9c9cde4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb477801-044d-438c-af06-290c7426d248",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43b025a8-61a4-47a8-9fe1-a5a33cabef25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb477801-044d-438c-af06-290c7426d248",
                    "LayerId": "e9c19745-d95c-4da0-b2d8-36652b1129bf"
                }
            ]
        },
        {
            "id": "9e9cce8c-8240-42b0-9874-7e699b303113",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "250dab8e-6580-4309-acfe-62756123cd76",
            "compositeImage": {
                "id": "0c34178f-630a-41c6-9a74-5cb85dc36976",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e9cce8c-8240-42b0-9874-7e699b303113",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e4546d7-ad0b-407e-98a9-ddabc4500628",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e9cce8c-8240-42b0-9874-7e699b303113",
                    "LayerId": "e9c19745-d95c-4da0-b2d8-36652b1129bf"
                }
            ]
        },
        {
            "id": "cd3bfc0a-9b9b-4882-bc86-8fb63f3fdb73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "250dab8e-6580-4309-acfe-62756123cd76",
            "compositeImage": {
                "id": "a77a1d5b-8bd3-4fb5-89b1-1191e6f32603",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd3bfc0a-9b9b-4882-bc86-8fb63f3fdb73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dcac3f7-17e2-451a-89f1-59b4024aba4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd3bfc0a-9b9b-4882-bc86-8fb63f3fdb73",
                    "LayerId": "e9c19745-d95c-4da0-b2d8-36652b1129bf"
                }
            ]
        },
        {
            "id": "90236b51-193f-4c42-b67c-60ebf858b72a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "250dab8e-6580-4309-acfe-62756123cd76",
            "compositeImage": {
                "id": "da9823dc-82be-44d4-8406-b7f24ad456af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90236b51-193f-4c42-b67c-60ebf858b72a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5c84cad-1d4b-4a74-b1fd-662f773f925b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90236b51-193f-4c42-b67c-60ebf858b72a",
                    "LayerId": "e9c19745-d95c-4da0-b2d8-36652b1129bf"
                }
            ]
        },
        {
            "id": "fc8f63a0-e04c-46a3-92e3-34cdd13d4f05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "250dab8e-6580-4309-acfe-62756123cd76",
            "compositeImage": {
                "id": "dd152f66-12d7-446b-a15b-8e32df88cc39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc8f63a0-e04c-46a3-92e3-34cdd13d4f05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0dc7dc9-3aa8-4309-bd27-6f45c2c286b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc8f63a0-e04c-46a3-92e3-34cdd13d4f05",
                    "LayerId": "e9c19745-d95c-4da0-b2d8-36652b1129bf"
                }
            ]
        },
        {
            "id": "f0d02790-cc4b-49f6-a8ee-12ed54900dde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "250dab8e-6580-4309-acfe-62756123cd76",
            "compositeImage": {
                "id": "2f9cae33-de82-40a8-b0b2-a005200642e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0d02790-cc4b-49f6-a8ee-12ed54900dde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a96a9666-71ef-473a-a258-220078ea3929",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0d02790-cc4b-49f6-a8ee-12ed54900dde",
                    "LayerId": "e9c19745-d95c-4da0-b2d8-36652b1129bf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "e9c19745-d95c-4da0-b2d8-36652b1129bf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "250dab8e-6580-4309-acfe-62756123cd76",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 12
}