{
    "id": "85ba1262-f21d-4444-9e35-576b7c8aafeb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_stand_in_room",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 959,
    "bbox_left": 0,
    "bbox_right": 539,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4db18ae8-9757-4201-a929-2aa4e310ae36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85ba1262-f21d-4444-9e35-576b7c8aafeb",
            "compositeImage": {
                "id": "2c782c72-5464-4bab-b8eb-83f48a0e0bf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4db18ae8-9757-4201-a929-2aa4e310ae36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27385966-926a-442b-b6b0-aecb7e641676",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4db18ae8-9757-4201-a929-2aa4e310ae36",
                    "LayerId": "e2fdd90f-1268-4e3e-9896-759f010ce994"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 960,
    "layers": [
        {
            "id": "e2fdd90f-1268-4e3e-9896-759f010ce994",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "85ba1262-f21d-4444-9e35-576b7c8aafeb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 540,
    "xorig": 687,
    "yorig": 580
}