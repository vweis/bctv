{
    "id": "ccbb6555-29ca-4356-8c3f-8b56837a2378",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_carpet_concreteTile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0cd2ab99-6dac-4069-b625-19f5d7cd898e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccbb6555-29ca-4356-8c3f-8b56837a2378",
            "compositeImage": {
                "id": "eddca7b2-f8c9-43d4-aa18-c75199cde7f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cd2ab99-6dac-4069-b625-19f5d7cd898e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71b5fab8-e176-46fa-9a59-1d10c2e2fc4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cd2ab99-6dac-4069-b625-19f5d7cd898e",
                    "LayerId": "fa2475f0-708f-47c2-a737-1c6d8614348b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fa2475f0-708f-47c2-a737-1c6d8614348b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ccbb6555-29ca-4356-8c3f-8b56837a2378",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}