{
    "id": "ab266edf-99b8-4ef3-9617-6b5db1210ffd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cubicle_raceCar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 17,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9d5d98a1-07db-49de-8c81-8f63515e6d31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab266edf-99b8-4ef3-9617-6b5db1210ffd",
            "compositeImage": {
                "id": "b70fcd60-5229-4df6-90f3-6d20d8ab1232",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d5d98a1-07db-49de-8c81-8f63515e6d31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54ce2468-e29b-4c52-880f-d2a7ed1438cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d5d98a1-07db-49de-8c81-8f63515e6d31",
                    "LayerId": "7f38c81c-f3d2-4da9-bdb2-65803071a4e8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "7f38c81c-f3d2-4da9-bdb2-65803071a4e8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ab266edf-99b8-4ef3-9617-6b5db1210ffd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 14
}