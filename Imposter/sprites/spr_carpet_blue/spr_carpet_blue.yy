{
    "id": "a1d37912-424c-4387-ae6d-48da9f75b2b4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_carpet_blue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5ee25f6c-065d-4db7-8a49-adb50d134ed4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1d37912-424c-4387-ae6d-48da9f75b2b4",
            "compositeImage": {
                "id": "1c7a665b-16d2-4a11-bc76-c3bef24fdadf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ee25f6c-065d-4db7-8a49-adb50d134ed4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cd385e9-2578-4ea9-8d56-2b64534423e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ee25f6c-065d-4db7-8a49-adb50d134ed4",
                    "LayerId": "da7d254d-6e7c-4ade-9d02-969625833503"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "da7d254d-6e7c-4ade-9d02-969625833503",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a1d37912-424c-4387-ae6d-48da9f75b2b4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}