{
    "id": "ba719966-3bc6-4492-adef-51071816f0ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_walking_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c1f196b5-d0fb-464a-9248-8e2f1990dfcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba719966-3bc6-4492-adef-51071816f0ea",
            "compositeImage": {
                "id": "4096bc2c-3afa-4fba-9130-0aed364238e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1f196b5-d0fb-464a-9248-8e2f1990dfcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c2af51f-7131-4384-84a0-fb2b418768e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1f196b5-d0fb-464a-9248-8e2f1990dfcc",
                    "LayerId": "d7d1f0e7-ca42-400b-9cb9-47bbef0effc8"
                }
            ]
        },
        {
            "id": "54bf2683-add2-429d-8753-9a955bd3c994",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba719966-3bc6-4492-adef-51071816f0ea",
            "compositeImage": {
                "id": "8e2ccf47-b01e-4a5d-a1db-d435a45ccaff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54bf2683-add2-429d-8753-9a955bd3c994",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bc09664-9b86-4ab7-89f1-836b90d62850",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54bf2683-add2-429d-8753-9a955bd3c994",
                    "LayerId": "d7d1f0e7-ca42-400b-9cb9-47bbef0effc8"
                }
            ]
        },
        {
            "id": "099461b2-2a98-4001-8897-7af7cb28821b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba719966-3bc6-4492-adef-51071816f0ea",
            "compositeImage": {
                "id": "d286a2fb-2740-4d80-9750-ed317cdd4e60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "099461b2-2a98-4001-8897-7af7cb28821b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4083b8bb-f49e-4d3c-ae1e-18b8ad605ac0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "099461b2-2a98-4001-8897-7af7cb28821b",
                    "LayerId": "d7d1f0e7-ca42-400b-9cb9-47bbef0effc8"
                }
            ]
        },
        {
            "id": "921f5cc8-ce52-4fcb-a249-9c43af2875f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba719966-3bc6-4492-adef-51071816f0ea",
            "compositeImage": {
                "id": "09ce6856-d9ca-4e5b-ac08-b3f87f17d406",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "921f5cc8-ce52-4fcb-a249-9c43af2875f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97f9854c-3615-46af-80c8-89629f80177b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "921f5cc8-ce52-4fcb-a249-9c43af2875f5",
                    "LayerId": "d7d1f0e7-ca42-400b-9cb9-47bbef0effc8"
                }
            ]
        },
        {
            "id": "7f96584a-48a8-48c3-a4e3-be44c0956637",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba719966-3bc6-4492-adef-51071816f0ea",
            "compositeImage": {
                "id": "c6a8454e-9d68-4a23-bb51-404c5a679f46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f96584a-48a8-48c3-a4e3-be44c0956637",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "265c1014-461c-42eb-9c50-2fb4e9b6b492",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f96584a-48a8-48c3-a4e3-be44c0956637",
                    "LayerId": "d7d1f0e7-ca42-400b-9cb9-47bbef0effc8"
                }
            ]
        },
        {
            "id": "fdcaec12-25c8-45c6-826e-fbbadc3a01fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba719966-3bc6-4492-adef-51071816f0ea",
            "compositeImage": {
                "id": "48793c67-11de-412b-964b-a91c63c4a851",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdcaec12-25c8-45c6-826e-fbbadc3a01fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d973aec-6aaf-4b4b-ae6b-619681820cf2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdcaec12-25c8-45c6-826e-fbbadc3a01fc",
                    "LayerId": "d7d1f0e7-ca42-400b-9cb9-47bbef0effc8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "d7d1f0e7-ca42-400b-9cb9-47bbef0effc8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba719966-3bc6-4492-adef-51071816f0ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 12
}