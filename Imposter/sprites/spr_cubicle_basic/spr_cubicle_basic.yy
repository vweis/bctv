{
    "id": "eeb9148f-047b-4be7-a8b5-191de4231691",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cubicle_basic",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 17,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "647c623d-1ae7-42ce-9593-a897056af617",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eeb9148f-047b-4be7-a8b5-191de4231691",
            "compositeImage": {
                "id": "a82cb2c3-d30e-4099-85d2-16471466eb41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "647c623d-1ae7-42ce-9593-a897056af617",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "997975c0-217a-4bc9-a3e3-b37746d9ea80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "647c623d-1ae7-42ce-9593-a897056af617",
                    "LayerId": "2d6df008-3792-40d8-af07-869fda556231"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "2d6df008-3792-40d8-af07-869fda556231",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eeb9148f-047b-4be7-a8b5-191de4231691",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 14
}