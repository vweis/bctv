{
    "id": "d89f484c-fe4c-42b7-923c-f1f146d858d3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_idle_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 22,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19ecf1a2-7921-4f8f-b2de-0bcb3531341f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d89f484c-fe4c-42b7-923c-f1f146d858d3",
            "compositeImage": {
                "id": "41b518cf-8102-412a-b13e-c16ca4c13bb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19ecf1a2-7921-4f8f-b2de-0bcb3531341f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e96e1a19-951c-4192-8adc-b0bdf83f2d01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19ecf1a2-7921-4f8f-b2de-0bcb3531341f",
                    "LayerId": "276387f6-3f97-4e79-ad6d-cb45a0a5ecd1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "276387f6-3f97-4e79-ad6d-cb45a0a5ecd1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d89f484c-fe4c-42b7-923c-f1f146d858d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 12
}