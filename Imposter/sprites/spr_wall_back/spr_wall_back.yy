{
    "id": "0a00bb7d-d738-4edd-a947-63f941ccc577",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6365407a-ee6a-4df0-aaf6-e8a25bf15125",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a00bb7d-d738-4edd-a947-63f941ccc577",
            "compositeImage": {
                "id": "014e23c2-a3e4-4acf-9743-1559b3901cce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6365407a-ee6a-4df0-aaf6-e8a25bf15125",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4b4c9d9-db32-48da-b247-b61da48183d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6365407a-ee6a-4df0-aaf6-e8a25bf15125",
                    "LayerId": "b581f729-8f02-481c-a001-9f93a412676c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "b581f729-8f02-481c-a001-9f93a412676c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0a00bb7d-d738-4edd-a947-63f941ccc577",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 32
}