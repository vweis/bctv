{
    "id": "69bf2234-1815-4b5e-9229-1acf611b55e5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_carpet_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f5a00ea4-bf23-4009-9f49-0f6f66961572",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69bf2234-1815-4b5e-9229-1acf611b55e5",
            "compositeImage": {
                "id": "fe252a32-d78a-43d2-a293-9732f77c007b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5a00ea4-bf23-4009-9f49-0f6f66961572",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1c39565-6298-4869-a86e-2bc38dbbbb7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5a00ea4-bf23-4009-9f49-0f6f66961572",
                    "LayerId": "5eb758f3-b6ce-4ccc-8dab-0986f5947db3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5eb758f3-b6ce-4ccc-8dab-0986f5947db3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "69bf2234-1815-4b5e-9229-1acf611b55e5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}