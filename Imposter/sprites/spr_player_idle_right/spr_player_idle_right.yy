{
    "id": "76c2aaf3-4201-41ce-ac18-4a2527146466",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_idle_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 22,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b57b982e-02ca-4641-a4d0-cfd555c683b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76c2aaf3-4201-41ce-ac18-4a2527146466",
            "compositeImage": {
                "id": "9fd5e92a-8094-4bd6-aecc-45ed15422679",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b57b982e-02ca-4641-a4d0-cfd555c683b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6b431a9-5045-41f1-9c0b-f20f3dd40fbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b57b982e-02ca-4641-a4d0-cfd555c683b3",
                    "LayerId": "8a398d7b-6a16-4dac-9868-7ac3d35f9931"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "8a398d7b-6a16-4dac-9868-7ac3d35f9931",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76c2aaf3-4201-41ce-ac18-4a2527146466",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 12
}