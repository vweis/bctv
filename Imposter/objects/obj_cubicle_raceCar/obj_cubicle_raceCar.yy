{
    "id": "15667816-06f5-475b-adf8-8fd2aa79237a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cubicle_raceCar",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3fad1774-aa3e-4538-97c0-3b7f2df68cda",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "ab266edf-99b8-4ef3-9617-6b5db1210ffd",
    "visible": true
}