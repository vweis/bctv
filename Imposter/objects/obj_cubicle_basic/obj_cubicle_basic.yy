{
    "id": "da13af42-7911-401a-b7ce-50fd4622d6b5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cubicle_basic",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3fad1774-aa3e-4538-97c0-3b7f2df68cda",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "eeb9148f-047b-4be7-a8b5-191de4231691",
    "visible": true
}