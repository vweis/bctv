{
    "id": "25a1f56f-d74b-47e6-9ad3-e54f731a4224",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cubicle_flower",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3fad1774-aa3e-4538-97c0-3b7f2df68cda",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "5e5ca886-2ea8-473b-921a-ba45ece98854",
    "visible": true
}