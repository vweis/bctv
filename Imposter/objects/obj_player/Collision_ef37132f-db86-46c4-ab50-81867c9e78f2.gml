var errorMarginInPixels = 2;

//check left, right, up and down for collisions
if place_free(x,y + errorMarginInPixels) {

  //code for collision with side of wall

  //Here, for instance, you would not set vspeed to 0, but hspeed to 0
  hspeed = 0;

} else {

  //code for collision with top of wall

  //here, for instance, you could set vspeed to 0.
  vspeed = 0;
}