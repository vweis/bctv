#macro speed_max 8





movementState = 0;
enum Directions {
	left = 180,
	right = 0,
	up = 90, 
	down = 270
}
enum MovementStates {
	neutral = 0,
	right = 1,
	up = 2, 
	left = 3,
	down = 4,
}
isMoving = false;
{
	var i;
	for (i = 2; i >= 0; i -= 1) {
		switch(i) {
			//for the first row, upward
			case 0:
				var j;
				for (j = 2; j >= 0; j -= 1) {
					directionInput[j,i] = MovementStates.up
				}
			
				break;
			//for the middle row, side to side or neutral
			case 1:
				var j;
				for (j = 2; j >= 0; j -= 1) {
					switch(j) {
						case 0:
							directionInput[j,i] = MovementStates.left
						break;
						case 1:
							directionInput[j,i] = MovementStates.neutral
						break;
						case 2:
							directionInput[j,i] = MovementStates.right
						break;
						default:
						break;
					}
				}
				
			break;
			//for the bottom row, downward
			case 2:
				var j;
				for (j = 2; j >= 0; j -= 1) {
					directionInput[j,i] = MovementStates.down
				}
			break;
			default:
				show_message("Did not initialize input matrix.")
			break;
		}
	}
}

//if !part_system_exists(global.PSystem) {
	global.PSystem = part_system_create();
//}


//--------Dialogue Stuff
reset_dialogue_defaults();
myPortrait			= spr_portrait_player;
myVoice				= snd_voice2;
myFont				= fnt_dialogue;
myName				= "Green";

myPortraitTalk		= spr_portrait_examplechar_mouth;
myPortraitTalk_x	= 26;
myPortraitTalk_y	= 44;
myPortraitIdle		= spr_portrait_examplechar_idle;
