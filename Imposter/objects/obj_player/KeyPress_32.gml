if (!instance_exists(obj_textbox)) {
	global.deformation -= 1;

	create_dialogue("Just a little " + randomize_string("monologue") + ". " + string(global.deformation)
	, -1);
}