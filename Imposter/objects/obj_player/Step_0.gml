if(instance_exists(obj_textbox)) {
	speed=0
	isMoving = false;
	exit
	};
	
playerSpeed = 0.07;

var hInput;
var vInput;

if (keyboard_check(vk_left) || keyboard_check_pressed(ord("A"))){
	hInput = 0;
} else if (keyboard_check(vk_right) || keyboard_check_pressed(ord("D"))){
	hInput = 2;
} else { hInput = 1; }

if (keyboard_check(vk_up) || keyboard_check_pressed(ord("W"))){
	vInput = 0;
} else if (keyboard_check(vk_down) || keyboard_check_pressed(ord("S"))){
	vInput = 2;
} else { vInput = 1; }

if (keyboard_check_pressed(vk_space)) {

}
//motion_add(direction, playerSpeed);


inputResult = directionInput[hInput,vInput]

movementState = inputResult

switch(movementState) {
	case MovementStates.neutral:
	//do nothing
	break;
	case MovementStates.left:
		if (inputResult != directionInput[1,1]) {
			//direction = Directions.left;
			motion_add(Directions.left, playerSpeed);
		}
	break;
	case MovementStates.right:
		if (inputResult != directionInput[1,1]) {
			//direction = Directions.right;
			motion_add(Directions.right, playerSpeed);
		}
	break;
	case MovementStates.up:
		if (inputResult != directionInput[1,1]) {
			//direction = Directions.up;
			motion_add(Directions.up, playerSpeed);
		}
	break;
	case MovementStates.down:
		if (inputResult != directionInput[1,1]) {
			
			//direction = Directions.down;
			motion_add(Directions.down, playerSpeed);
			
		}
	break;
	default:
	
	break;
}



//Friction is applied

if (isMoving) {
	
	speed = speed * (970/1000) - 0.02;
	
	}

if speed <= 0 {
	speed = 0;
	isMoving = false;
	} else { isMoving = true }





if (speed > speed_max) {
speed = speed_max
}