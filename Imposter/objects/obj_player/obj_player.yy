{
    "id": "f6695d03-147f-416b-8bd5-9226b97d3452",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "031df69c-e081-49fa-abb2-3343d391adfc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f6695d03-147f-416b-8bd5-9226b97d3452"
        },
        {
            "id": "a14df42b-f035-468d-b9eb-4a317deccbda",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f6695d03-147f-416b-8bd5-9226b97d3452"
        },
        {
            "id": "ef37132f-db86-46c4-ab50-81867c9e78f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "0d81b6af-9494-4c23-a82e-72c27f97b97f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f6695d03-147f-416b-8bd5-9226b97d3452"
        },
        {
            "id": "b8ae7267-c309-4162-bde2-f87450d3b4b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "f6695d03-147f-416b-8bd5-9226b97d3452"
        },
        {
            "id": "0edc3d04-a239-475b-b191-a0bb1ba66a5d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "f6695d03-147f-416b-8bd5-9226b97d3452"
        },
        {
            "id": "c1701825-7b70-4086-9051-aeb6ad1944d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "f6695d03-147f-416b-8bd5-9226b97d3452"
        }
    ],
    "maskSpriteId": "9cf799d7-b493-4a0f-aac1-4ff50e594288",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9cf799d7-b493-4a0f-aac1-4ff50e594288",
    "visible": true
}