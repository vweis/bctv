{
    "id": "ba96caf2-c9bd-48cd-8dc7-010c65ca7184",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_chair_legs",
    "eventList": [
        {
            "id": "83381813-342d-4dc4-a164-1601506cc946",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ba96caf2-c9bd-48cd-8dc7-010c65ca7184"
        },
        {
            "id": "2bd23b4c-6434-4054-b673-d76d4e14ab9e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ba96caf2-c9bd-48cd-8dc7-010c65ca7184"
        },
        {
            "id": "38e238e9-c90e-4350-a5cc-2c02244f10e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "ba96caf2-c9bd-48cd-8dc7-010c65ca7184"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bacfa89f-a1ce-4276-93c3-7cba4cb8ddb1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7d111430-2381-4f3e-a16f-24758970fe52",
    "visible": true
}