event_inherited();

//-------DIALOGUE STUFF

myPortrait			= spr_radiator;
myVoice				= snd_voice1;
myName				= "Radiator";

/*myPortraitTalk		= spr_portrait_examplechar_mouth;
myPortraitTalk_x	= 26;
myPortraitTalk_y	= 44;
myPortraitIdle		= spr_portrait_examplechar_idle;*/


//-------OTHER

destructionIsTriggered = false; //Used to determine when the instance is destroyed.

choice_variable		= -1;	//the variable we change depending on the player's choice in dialogue