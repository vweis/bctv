{
    "id": "a5fda729-dc4a-4de7-ac99-c7431241a677",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_chair_back",
    "eventList": [
        {
            "id": "5bc1f72b-59c6-4203-995b-3190709188d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a5fda729-dc4a-4de7-ac99-c7431241a677"
        },
        {
            "id": "bcde8314-a8f7-4670-8950-3308dec5ebc2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a5fda729-dc4a-4de7-ac99-c7431241a677"
        },
        {
            "id": "602239d0-eaf8-4822-b8ab-be7827180c3d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "a5fda729-dc4a-4de7-ac99-c7431241a677"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bacfa89f-a1ce-4276-93c3-7cba4cb8ddb1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7d111430-2381-4f3e-a16f-24758970fe52",
    "visible": true
}