{
    "id": "adab33e6-ff3e-4ebb-9585-de0c621c0fc0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cubicle_tech",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3fad1774-aa3e-4538-97c0-3b7f2df68cda",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "c302d173-c231-4c44-843d-5f33ccb40488",
    "visible": true
}