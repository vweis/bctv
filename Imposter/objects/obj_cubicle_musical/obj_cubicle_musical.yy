{
    "id": "95134bb2-f246-4383-943e-67360ff2bf04",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cubicle_musical",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3fad1774-aa3e-4538-97c0-3b7f2df68cda",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "d33b966c-0cb0-4707-a6a4-cc5cf105520d",
    "visible": true
}