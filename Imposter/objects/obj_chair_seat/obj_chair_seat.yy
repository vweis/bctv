{
    "id": "90c61eae-fe11-45e4-b288-d5b09aca0939",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_chair_seat",
    "eventList": [
        {
            "id": "1bd6b4f8-573a-487c-b382-c3773588f3e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "90c61eae-fe11-45e4-b288-d5b09aca0939"
        },
        {
            "id": "5f0117c8-81af-4070-bb08-03af13db850f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "90c61eae-fe11-45e4-b288-d5b09aca0939"
        },
        {
            "id": "9b236735-d104-4e49-8506-e720e7480684",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "90c61eae-fe11-45e4-b288-d5b09aca0939"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bacfa89f-a1ce-4276-93c3-7cba4cb8ddb1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7d111430-2381-4f3e-a16f-24758970fe52",
    "visible": true
}