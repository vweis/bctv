{
    "id": "45a7ab9f-ea4d-40f4-ad15-fe22ad1727f0",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_dialogue",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Lucida Console",
    "glyphOperations": 8,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "556b1acc-e5e6-4c5f-88b3-9b6a35c8aa7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "473189ed-a20d-4e85-8c31-b9aed82049c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 25,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 190,
                "y": 164
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "16cf40b3-764f-4404-8c1e-0f6575165f47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 178,
                "y": 164
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "a28bdef6-8a15-4d8d-9535-8425e4920f2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 162,
                "y": 164
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "d9152ea9-50d5-4996-91e7-7f17c27b75b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 149,
                "y": 164
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "8946fe91-1d4a-4c85-a4e3-5df4ef45c472",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 132,
                "y": 164
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "98d180ec-53ef-48df-ab24-37632a228ff3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 115,
                "y": 164
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "9bf696e3-1f9d-44d3-bfcb-f59ded697c23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 25,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 109,
                "y": 164
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "a4c2f3ab-6311-43bb-8716-9c93ababd7a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 25,
                "offset": 4,
                "shift": 14,
                "w": 9,
                "x": 98,
                "y": 164
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "b966cc2c-385d-4a1e-b14a-6a2308b1463b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 10,
                "x": 86,
                "y": 164
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "c8245ca8-d549-4761-9bbf-6c83d03f54ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 72,
                "y": 164
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "4cf4be4d-5306-4a53-9479-d47e13ed9b88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 57,
                "y": 164
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "d666b49f-a283-4aa0-8441-a80d019f4209",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 25,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 51,
                "y": 164
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "dcaac5c8-b22b-4e8f-9699-b4d04616f8da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 39,
                "y": 164
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "c38b1a1f-68f2-4895-b6f0-c9efad763e49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 25,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 33,
                "y": 164
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "352c736b-16f4-40c7-880d-2774118baddd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 17,
                "y": 164
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "2cc18ae2-12db-4d08-9b5f-e504b0f2b04a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 164
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "2576b5a8-d898-4296-a7e6-c5d0c2db5af5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 488,
                "y": 137
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "1ace8429-cddb-4eab-b1cb-03af050053f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 196,
                "y": 164
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d72e3878-9728-4d34-9f84-40dfdcc39c5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 223,
                "y": 164
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "c5b233e4-3745-4c9e-bb56-a2f93bb32c76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 481,
                "y": 164
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "5cbd47ee-7d7e-42d1-b733-25886b74ebb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 25,
                "offset": 3,
                "shift": 14,
                "w": 10,
                "x": 236,
                "y": 164
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "bc62f3ae-9422-4469-bfc5-6df9acbc2e88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 466,
                "y": 164
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "2e79bd0e-45a4-4f65-bf4d-bfee1455ecc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 453,
                "y": 164
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "463862ab-73f9-41ad-a8e3-b2f2b274b1ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 439,
                "y": 164
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "41395ad5-8c67-4fe1-a40a-58c4fb208cad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 425,
                "y": 164
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "9e00c1ff-0e7e-4f4a-8801-f02d10f86300",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 25,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 419,
                "y": 164
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "b12a5795-3de0-45c8-b48f-15a9abb0ec01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 25,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 413,
                "y": 164
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "08ecdb98-c480-48c1-bbd4-c4e1c08f91c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 398,
                "y": 164
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "1dd3dac0-7b31-4ca9-8daf-3508d6411c6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 383,
                "y": 164
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "e666b4e0-0230-4969-8a1d-bd0efd783c2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 368,
                "y": 164
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "31bad6e6-9406-4da3-b82a-5c92754e6eb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 354,
                "y": 164
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "a7642ea5-c343-4dce-ab51-a244182ec677",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 337,
                "y": 164
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "78cc5897-1e1e-4ce4-87f0-4c9fe8426548",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 320,
                "y": 164
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "22a241f5-36ef-417f-ab77-3540bf6a5b2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 306,
                "y": 164
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "8c2506e3-23e8-43fe-b46c-8c0d1f163357",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 291,
                "y": 164
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "cdae218a-a129-42d0-81ee-24948bc7c5a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 276,
                "y": 164
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b787f480-595f-4815-b7c1-d9076d84341c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 262,
                "y": 164
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "110749b7-8180-454c-80ad-72877eaab169",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 248,
                "y": 164
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "74105f83-78a8-4efb-a5f9-38bdefb2af20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 472,
                "y": 137
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "89b9e1cc-4af1-4c40-a0f4-bab510c98113",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 458,
                "y": 137
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "7038d4a4-b6fa-4184-8e2e-dfbbf746225f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 445,
                "y": 137
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "14fa9e47-e9ac-4aee-ad11-23dc0e4be931",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 9,
                "x": 434,
                "y": 137
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "bab50558-1aa4-465e-9bcb-dd86d0552507",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 13,
                "x": 134,
                "y": 137
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "67b6af3c-329f-4fb4-8c12-8690601a7437",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 120,
                "y": 137
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a3962633-f777-46f3-a655-1ba49265e8e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 105,
                "y": 137
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "b8efc157-80cd-44b0-ac24-ba21b68ff982",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 91,
                "y": 137
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "e4611555-14c8-4a60-9893-97abce81cb0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 75,
                "y": 137
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "813a84c6-a916-4c3f-a21b-92c1f32997f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 61,
                "y": 137
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "76e01347-b96c-48fd-86d4-cc068a66a4b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 44,
                "y": 137
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "d1b3143e-7fcd-4ea1-97f4-c4034ab39a60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 13,
                "x": 149,
                "y": 137
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "1b2369c9-ff74-48cd-bd91-c334dbb79d19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 29,
                "y": 137
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "33891674-36f3-4a5a-a5e3-d82e4e8d67ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 2,
                "y": 137
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "4ae79ff4-1d76-45d3-9bfe-7b8ebd7dc0f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 483,
                "y": 110
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "5efa1900-9a79-4283-ba7e-fb4559d1a6b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 466,
                "y": 110
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "518c65ac-6d62-4c15-abe6-57ddb481da80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 449,
                "y": 110
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "452b1d6e-0533-4f81-9b55-a9a142e720a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 432,
                "y": 110
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "9a7f3ce2-12be-434c-a12d-adc722e2f0dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 415,
                "y": 110
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "dd4f344d-3b7b-41bf-83f6-23a1aaea5e0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 400,
                "y": 110
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "d08f16c6-fa25-46a4-8283-191c99c10a60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 25,
                "offset": 5,
                "shift": 14,
                "w": 8,
                "x": 19,
                "y": 137
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "353903ea-9ed3-4ebe-bb6f-88cdc9655600",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 164,
                "y": 137
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "cbb22e0f-45a2-443f-8948-0a6bc9bf1b38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 8,
                "x": 180,
                "y": 137
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "63af75a6-180c-4bc4-bee2-fde70e9fa119",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 190,
                "y": 137
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "96915c70-9a14-4675-a19d-75510f5b7ed9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 417,
                "y": 137
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "49cd947b-233c-4331-a3bf-d2ff81a90330",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 25,
                "offset": 4,
                "shift": 14,
                "w": 7,
                "x": 408,
                "y": 137
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "9bb07d16-b3fd-47a3-be4b-e0ea947bfc32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 393,
                "y": 137
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "fd226f70-fb03-450f-aff4-bad81dfa6a9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 379,
                "y": 137
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "4cf4157d-8900-4fa8-937b-a777a5ec8f68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 365,
                "y": 137
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "68955403-a5ea-48c2-9a02-0d897a182841",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 351,
                "y": 137
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "cad8ea6e-55df-4301-8d75-6879c5fd61ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 337,
                "y": 137
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "d1820173-f609-4596-854e-bd34c2b1b54d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 14,
                "x": 321,
                "y": 137
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "1957d970-d19a-4c5d-ae75-9f52bac1ca62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 307,
                "y": 137
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "d85ad7c9-32e2-424d-9b62-eae3a891c5f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 294,
                "y": 137
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "8981fe53-89db-441c-b04d-50bbd49a7664",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 8,
                "x": 284,
                "y": 137
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "9c0e5be9-08c9-4474-bd0b-bc2e6f355589",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 10,
                "x": 272,
                "y": 137
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "a122ffa1-59d5-423f-92fe-be617880000e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 258,
                "y": 137
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "b81911f4-52b3-45fc-9bd5-1486d2b795b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 8,
                "x": 248,
                "y": 137
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "8b6b7ed4-5f8d-4f88-91fd-22747afcde39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 233,
                "y": 137
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "47a50c54-f626-4c24-831e-4dffb754507a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 220,
                "y": 137
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "9013166d-da17-44e5-9807-667584adfb5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 205,
                "y": 137
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "ea3c16c9-d30b-4e75-9219-e62a1544b09c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 209,
                "y": 164
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "91645fb1-484c-4fe8-b31d-954a57cd22a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 19,
                "y": 191
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "fa71e94d-ad5f-48a6-9871-97361b989e59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 25,
                "offset": 3,
                "shift": 14,
                "w": 11,
                "x": 382,
                "y": 218
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "e0a654c2-7171-4f10-9f59-c0eaebe183d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 33,
                "y": 191
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "8f3f5f28-932b-4437-8522-47e70b8cd189",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 363,
                "y": 218
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "656b6e47-8a5b-4d6a-a94d-8db782b97afc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 350,
                "y": 218
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "982ffc4d-a436-4676-b299-d21754415d74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 334,
                "y": 218
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "108bb353-2007-4d62-8313-0d4adeecc9d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 317,
                "y": 218
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "9bf2a6d4-0d08-496e-8a88-6d8e2c0cc21e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 302,
                "y": 218
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "e9094244-b140-4751-b22f-5455ab0a1b44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 287,
                "y": 218
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "38ec44fd-289b-4cac-a648-c8e52016d9a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 273,
                "y": 218
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "cbbe7d4c-9644-424a-9b65-d4bcd4f5b241",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 261,
                "y": 218
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "04ae7df6-263d-4ecb-88d1-0b8ece6e529a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 25,
                "offset": 6,
                "shift": 14,
                "w": 3,
                "x": 256,
                "y": 218
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "72a876f4-b99e-4fcd-a093-7e4d16c69093",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 244,
                "y": 218
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "24238b16-d201-4d53-a057-cd66619f546e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 229,
                "y": 218
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "80786f2d-4ed1-4fbf-8890-d8bfa36f015c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 0,
                "x": 227,
                "y": 218
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "c9b6f027-3317-4526-a83a-f6aa58087525",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 25,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 221,
                "y": 218
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "d0a02027-3ef1-45ed-b97f-49648b3c1b1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 209,
                "y": 218
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "96cb37d7-e3d1-4e87-af70-8d29f25971ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 196,
                "y": 218
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "0513f6aa-2c51-4f85-98ed-dc2d5dd922eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 181,
                "y": 218
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "3c82fe80-7d1c-4170-a7da-a615d94e5dcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 164,
                "y": 218
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "eb38b239-e036-41d1-be17-8bef29356c53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 25,
                "offset": 6,
                "shift": 14,
                "w": 3,
                "x": 377,
                "y": 218
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "84837ff2-5f5a-4e3a-a4e9-b69b6d0aaa51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 395,
                "y": 218
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "a7f3a1c6-727b-4a21-852d-b26357dcdabf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 25,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 120,
                "y": 245
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "1e19ecca-94a0-4b79-9eb0-afc50a6be6b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 408,
                "y": 218
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "1267045d-4a35-4d0a-9bf6-137542e2486a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 107,
                "y": 245
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "5f4a818f-4ec2-4483-a2c6-46b7453107b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 93,
                "y": 245
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "49b05c9e-406d-4f1f-87c9-9db7b4ddb3c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 78,
                "y": 245
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "ec7ad01f-5fbe-4790-b960-20aee71212b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 66,
                "y": 245
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "346fac0c-850e-4c66-8911-a1856f569b74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 53,
                "y": 245
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "6668ab92-785a-481b-8d14-296db3e1ac80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 36,
                "y": 245
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "d370fe07-5a03-4397-8ff0-e69e319d578a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 25,
                "offset": 4,
                "shift": 14,
                "w": 6,
                "x": 28,
                "y": 245
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "be0281fb-3e96-48ae-b9e4-9d152ce0c779",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 13,
                "y": 245
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "bbf759af-b7f2-4626-ad56-4bc16948e414",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 25,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 2,
                "y": 245
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "e26a0225-14c6-4281-b5cb-1fc0051fb157",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 25,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 492,
                "y": 218
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "338ddc48-93ca-44ac-a85d-1c768ed8dece",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 25,
                "offset": 4,
                "shift": 14,
                "w": 7,
                "x": 483,
                "y": 218
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "cf35df5e-663c-42cc-886a-a72e13b14c3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 469,
                "y": 218
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "2a1f7914-cbd4-4f01-8a64-f8429727de15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 456,
                "y": 218
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "5065e623-1403-4a9f-b875-454b66a669b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 25,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 450,
                "y": 218
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "62371102-c01f-48e2-86af-f969d58cd38b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 25,
                "offset": 5,
                "shift": 14,
                "w": 5,
                "x": 443,
                "y": 218
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "a2b3a366-5fa5-4dc3-9695-07424582f209",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 25,
                "offset": 4,
                "shift": 14,
                "w": 5,
                "x": 436,
                "y": 218
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "de51ecc6-846e-4089-8edc-32841d541674",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 424,
                "y": 218
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "e11828e9-c348-4471-8691-47bcff387d4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 149,
                "y": 218
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "10872860-5146-4964-b87a-2ca671920bba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 132,
                "y": 218
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "ef3245eb-45a6-4afd-b840-f15d04c1d0d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 115,
                "y": 218
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "0453bb31-be8f-4145-a8c2-608aaf8acf74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 98,
                "y": 218
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "8be1c375-124a-4110-8e64-7e275e8a421b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 271,
                "y": 191
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "34557bdb-e34e-4742-935e-e931586ca2ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 254,
                "y": 191
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "4a5cb9b2-f650-46ab-8a54-dc9709304260",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 237,
                "y": 191
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "ddc8d738-3d94-4ab7-96a4-ca696d222c16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 220,
                "y": 191
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "e7d1b799-c609-43c5-8cc2-cef523c9377a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 203,
                "y": 191
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "1cbee129-919b-4b84-ab37-f40b2c06e420",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 186,
                "y": 191
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "d709c4b6-4a9a-49ef-8a91-ce44634d69f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 169,
                "y": 191
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "facc521b-8336-4de2-a00d-591f423ff034",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 285,
                "y": 191
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "5d1c1fdd-a5ce-430c-93e9-cb372e3c21f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 154,
                "y": 191
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "997ae9f1-806d-48ac-86e0-95309ec9a5be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 127,
                "y": 191
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "1d42bb74-8baa-42d2-ab3e-6866da123dbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 113,
                "y": 191
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "3e4a9ff7-08e5-41fc-9e62-0f209f14b7b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 99,
                "y": 191
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "d2f8bff9-75de-4052-922e-2c263a4eb5cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 85,
                "y": 191
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "d2298232-240e-4abe-99f8-28d6a933dc0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 72,
                "y": 191
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "9e011a3e-b054-4e77-be6f-8ea779e4f8f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 59,
                "y": 191
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "f51c8923-7906-4316-a975-f960cdb95510",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 46,
                "y": 191
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "0bd0a2eb-a06e-4134-b7b2-7a0749e10137",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 141,
                "y": 191
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "c889165c-de53-4ab8-a649-d2a39aba6c51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 302,
                "y": 191
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "8a44866f-bede-426b-a312-2bd15ae88a3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 318,
                "y": 191
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "762a3e92-5db0-4054-ba41-2af6dbd8b38a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 332,
                "y": 191
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "d563b091-f610-4bdb-8b0a-f8a4db1f0211",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 82,
                "y": 218
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "861f23fb-e66f-4515-8d22-cfbc44eebd41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 66,
                "y": 218
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "3e991afa-f344-475b-90b7-09e4e4a4308a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 50,
                "y": 218
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "b8987932-dbf2-45d4-823b-c1ce99379104",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 34,
                "y": 218
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "6ea5e9dd-82d0-4ed7-a6d7-ab10cce5310b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 18,
                "y": 218
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "831485d0-f1ce-4b16-842c-719a0b3e005e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 218
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "91be288e-4a0e-4925-a69d-dc5832023e4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 495,
                "y": 191
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "b009cd6b-22d9-46d9-ab17-983c485685bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 481,
                "y": 191
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "648ef304-ef80-4752-bcbf-bb74e1d083e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 467,
                "y": 191
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "3cef92af-5741-4811-869a-68c7b4e50553",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 453,
                "y": 191
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "fb232a4c-3c7c-4129-8a39-d2ce0babbaf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 436,
                "y": 191
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "6b67dafa-0ecd-49e4-9c77-4287e2f44303",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 422,
                "y": 191
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "5db89b39-38f1-40bf-b06c-fdbb7ea97bcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 408,
                "y": 191
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "09d0035f-315d-481d-81b9-5de938ba3696",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 393,
                "y": 191
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "af745fd5-8818-4419-872a-2e032c1c84e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 378,
                "y": 191
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "19717f34-216d-4ac6-9d07-3123da2a7b82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 363,
                "y": 191
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "a783e8f4-1aec-46a2-a07e-1940cedbdd2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 348,
                "y": 191
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "b57dfd96-461f-434b-b690-e4a423a48f1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 385,
                "y": 110
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "ecb36b2d-0a0a-445a-9f9d-ce8c1d3e209d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 370,
                "y": 110
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "0c88b4e0-96aa-4403-a93b-d12423c6deb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 353,
                "y": 110
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "24fabdad-84b0-4f2a-a849-df929b54a53f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 44,
                "y": 110
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "b62695c0-6131-4e69-a436-15fbd78272fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 302,
                "y": 29
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "8bd735ef-b583-41f6-b82a-d4b603a706f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 288,
                "y": 29
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "827b6c3e-a816-4fb5-8f6d-d60cd0fee3a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 274,
                "y": 29
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "4096a985-4506-4d51-8deb-ceff558b4d73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 260,
                "y": 29
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "32e270e8-9940-4923-8b49-5bb01268675c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 8,
                "x": 250,
                "y": 29
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "4675afb7-ab16-44b1-ae1e-72d53564382f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 238,
                "y": 29
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "c1a9e581-e165-4da5-8bdc-9f5046fde630",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 226,
                "y": 29
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "b93f6d3d-7827-49b9-81bd-f1d8804a1c2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 9,
                "x": 316,
                "y": 29
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "400de639-59d2-492f-88c4-21e303ca1943",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 211,
                "y": 29
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "46488f4d-f397-4579-920c-dbcff98eaadc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 183,
                "y": 29
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "0d861965-5067-44a7-98ea-9455a62bd356",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 168,
                "y": 29
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "84e13ff5-74c5-496a-b7c7-5aa2ffc60971",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 153,
                "y": 29
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "a6534afb-70b5-4ccb-8cc0-934685c9b399",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 138,
                "y": 29
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "4df1b568-f15f-4d5e-9ec7-3872b0bca090",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 123,
                "y": 29
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "f3838fc3-ea13-4ee9-a8e2-8649a5b9dcbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 108,
                "y": 29
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "1aace2ed-d168-4499-94f3-c9767230421b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 93,
                "y": 29
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "9afcee09-9428-4bb5-bf4f-5c5c75ca75df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 196,
                "y": 29
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "11727522-9531-476e-80c2-76a73b116a86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 327,
                "y": 29
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "a0454fd1-bc11-4e7b-a1d9-eee315138064",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 340,
                "y": 29
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "10676187-703b-43b2-ab8c-577fcbe10f74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 353,
                "y": 29
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "db604c5f-c73d-4d2f-a9a6-4932331e85fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 110,
                "y": 56
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "32694fa0-a010-483c-9efd-fad6921b3ade",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 95,
                "y": 56
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "1e623a4a-3d37-43fe-86f7-a49a2692fdaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 81,
                "y": 56
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "b009ed69-7604-4cd9-a008-7bf79c1c347a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 66,
                "y": 56
            }
        },
        {
            "Key": 256,
            "Value": {
                "id": "51a5c1ed-d9e1-4ca6-8083-bed1f6a8551e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 256,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 49,
                "y": 56
            }
        },
        {
            "Key": 257,
            "Value": {
                "id": "1e52dd95-267e-40e2-9758-ac2cdaa585e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 257,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 34,
                "y": 56
            }
        },
        {
            "Key": 258,
            "Value": {
                "id": "2250d528-75ad-472b-b643-df5c1a1f355c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 258,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 17,
                "y": 56
            }
        },
        {
            "Key": 259,
            "Value": {
                "id": "2c2d8310-5488-42a5-8d8b-a78917df7c57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 259,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 260,
            "Value": {
                "id": "b9528098-b417-4ef8-b174-f627fd61e60f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 260,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 483,
                "y": 29
            }
        },
        {
            "Key": 261,
            "Value": {
                "id": "6bf036f9-f97e-41f9-9a71-d7c50c74f43f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 261,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 468,
                "y": 29
            }
        },
        {
            "Key": 262,
            "Value": {
                "id": "809a1619-e3ee-4cb1-8eff-fcf58a51f69b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 262,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 453,
                "y": 29
            }
        },
        {
            "Key": 263,
            "Value": {
                "id": "f1eee7f2-bfad-4407-bd95-2d6daa9bd14b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 263,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 439,
                "y": 29
            }
        },
        {
            "Key": 264,
            "Value": {
                "id": "2d6d43bf-a6df-4193-adee-4eb634fdbd0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 264,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 424,
                "y": 29
            }
        },
        {
            "Key": 265,
            "Value": {
                "id": "6a8a6129-15f0-47ae-a576-27361d0a8231",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 265,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 410,
                "y": 29
            }
        },
        {
            "Key": 266,
            "Value": {
                "id": "99dd4793-a8eb-4f1b-87d3-ccb167d60bce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 266,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 395,
                "y": 29
            }
        },
        {
            "Key": 267,
            "Value": {
                "id": "b12e2271-0ba6-4613-b4ac-f3a0949c44b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 267,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 381,
                "y": 29
            }
        },
        {
            "Key": 268,
            "Value": {
                "id": "cd816ade-4b27-48c8-a7b2-844ce52d3b7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 268,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 366,
                "y": 29
            }
        },
        {
            "Key": 269,
            "Value": {
                "id": "51b0afdc-4333-40fe-ab71-837e75cb3429",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 269,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 79,
                "y": 29
            }
        },
        {
            "Key": 270,
            "Value": {
                "id": "7636554e-2350-4319-be70-e2959fe3787d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 270,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 123,
                "y": 56
            }
        },
        {
            "Key": 271,
            "Value": {
                "id": "e74a049b-176d-42ff-b52d-585e8536da5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 271,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 64,
                "y": 29
            }
        },
        {
            "Key": 272,
            "Value": {
                "id": "39aea04c-af4e-40b3-a397-114508cf73f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 272,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 33,
                "y": 29
            }
        },
        {
            "Key": 273,
            "Value": {
                "id": "8a34809f-2e84-4a4c-9d17-e4be1b8f21f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 273,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 14,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 274,
            "Value": {
                "id": "ffdad6a2-407c-4e66-8bc7-c797f4d8023f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 274,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 220,
                "y": 2
            }
        },
        {
            "Key": 275,
            "Value": {
                "id": "361af0cd-ceea-4f40-aad9-387a6b596dea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 275,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 276,
            "Value": {
                "id": "e69b58c1-559a-44f6-930a-53714339ed4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 276,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 277,
            "Value": {
                "id": "36b95018-706d-4f2c-821a-af0bc085a253",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 277,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 278,
            "Value": {
                "id": "24189716-3b01-46e7-9756-3db1404e0508",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 278,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 279,
            "Value": {
                "id": "f23baa0f-8e00-4b1d-8acc-75dc9cf9955f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 279,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 280,
            "Value": {
                "id": "6107acc5-5279-42e6-985c-be7939f041e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 280,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 250,
                "y": 2
            }
        },
        {
            "Key": 281,
            "Value": {
                "id": "e6f7627b-664d-4f62-a122-3a01b6d77d98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 281,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 136,
                "y": 2
            }
        },
        {
            "Key": 282,
            "Value": {
                "id": "81ac87ff-9e33-4d57-942c-f2f75f9338ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 282,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 283,
            "Value": {
                "id": "b75e270a-f3a4-40fb-94da-213a8477a878",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 283,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 284,
            "Value": {
                "id": "14a39cce-6058-48e4-bb5a-90ea7604003c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 284,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 285,
            "Value": {
                "id": "05e3d2ad-471a-46e5-a19c-651495ca8a3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 285,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 286,
            "Value": {
                "id": "62cde0db-26ed-4173-8ae7-f10bf2bc5fb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 286,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 287,
            "Value": {
                "id": "ecbb2fb5-67f4-477e-b2d5-c8389d185710",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 287,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 288,
            "Value": {
                "id": "dd65185a-9e4e-4819-9b42-07d3be9b73e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 288,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 289,
            "Value": {
                "id": "7bbac728-2bcf-4e59-9a15-35ea3628a852",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 289,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 290,
            "Value": {
                "id": "90cbcbf4-b803-4e4d-a798-5a1ca8115d2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 290,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 264,
                "y": 2
            }
        },
        {
            "Key": 291,
            "Value": {
                "id": "9b1c937f-c286-4ea5-b4c1-f3f3e881470f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 291,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 280,
                "y": 2
            }
        },
        {
            "Key": 292,
            "Value": {
                "id": "ef15f90d-9849-4f33-aca8-2006053b6608",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 292,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 294,
                "y": 2
            }
        },
        {
            "Key": 293,
            "Value": {
                "id": "52b801cb-4ce0-4589-9fc5-86329c4ff9af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 293,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 19,
                "y": 29
            }
        },
        {
            "Key": 294,
            "Value": {
                "id": "ff9dd445-5885-49bf-bf99-3aaf1a2b5ad2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 294,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 2,
                "y": 29
            }
        },
        {
            "Key": 295,
            "Value": {
                "id": "93acd172-9fae-46e8-b64c-9cb3a521fecf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 295,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 481,
                "y": 2
            }
        },
        {
            "Key": 296,
            "Value": {
                "id": "c0a3d6c1-fca0-4f55-8f8c-6f0f802648ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 296,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 468,
                "y": 2
            }
        },
        {
            "Key": 297,
            "Value": {
                "id": "5e9da8bf-dcfe-4b4f-a53f-6a937da75ea8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 297,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 8,
                "x": 458,
                "y": 2
            }
        },
        {
            "Key": 298,
            "Value": {
                "id": "e1124db1-3986-4ba6-83b1-e1e53f3646c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 298,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 445,
                "y": 2
            }
        },
        {
            "Key": 299,
            "Value": {
                "id": "1de0f2fc-a99b-47d9-8714-71f827561e1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 299,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 433,
                "y": 2
            }
        },
        {
            "Key": 300,
            "Value": {
                "id": "490c5a3d-f917-47cc-8268-8aa8cd7c0106",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 300,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 420,
                "y": 2
            }
        },
        {
            "Key": 301,
            "Value": {
                "id": "42b95f66-012a-4891-a35d-e2f88c88983b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 301,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 408,
                "y": 2
            }
        },
        {
            "Key": 302,
            "Value": {
                "id": "88188b3c-c108-4f81-81be-427f29ff2d97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 302,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 395,
                "y": 2
            }
        },
        {
            "Key": 303,
            "Value": {
                "id": "41a52e21-4c1f-4ac5-8f44-3b269a9e982d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 303,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 8,
                "x": 385,
                "y": 2
            }
        },
        {
            "Key": 304,
            "Value": {
                "id": "86c8b0be-3e79-424e-8fbb-218ba34a68b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 304,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 372,
                "y": 2
            }
        },
        {
            "Key": 305,
            "Value": {
                "id": "f3a9aa7d-323e-45c2-bd57-f27a3143beeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 305,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 7,
                "x": 363,
                "y": 2
            }
        },
        {
            "Key": 306,
            "Value": {
                "id": "218356e9-f47a-4d6a-a7f0-3a81d3257186",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 306,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 347,
                "y": 2
            }
        },
        {
            "Key": 307,
            "Value": {
                "id": "5be9efc8-05d0-48f1-bb86-0efc786d0a38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 307,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 332,
                "y": 2
            }
        },
        {
            "Key": 308,
            "Value": {
                "id": "57b9708e-9e1a-4b3a-a43b-72dfae0c30bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 308,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 320,
                "y": 2
            }
        },
        {
            "Key": 309,
            "Value": {
                "id": "7f7e001e-e99b-4e67-a888-bdb28d452cc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 309,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 10,
                "x": 308,
                "y": 2
            }
        },
        {
            "Key": 310,
            "Value": {
                "id": "68063354-ecfb-4cec-b019-8600410203b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 310,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 13,
                "x": 49,
                "y": 29
            }
        },
        {
            "Key": 311,
            "Value": {
                "id": "87fd3844-f485-4f3a-826f-dc68cf42e6ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 311,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 138,
                "y": 56
            }
        },
        {
            "Key": 312,
            "Value": {
                "id": "15c9bf8c-d628-40b1-a2d9-6f9359e721b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 312,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 152,
                "y": 56
            }
        },
        {
            "Key": 313,
            "Value": {
                "id": "1e721d79-d330-47ee-bb0e-0f7841e4aa93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 313,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 165,
                "y": 56
            }
        },
        {
            "Key": 314,
            "Value": {
                "id": "03aa071a-d31b-473c-b3d3-8d0e08bbf67f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 314,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 13,
                "x": 16,
                "y": 110
            }
        },
        {
            "Key": 315,
            "Value": {
                "id": "2893a600-55ce-4fbc-8ebd-b6adb1c22551",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 315,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 110
            }
        },
        {
            "Key": 316,
            "Value": {
                "id": "bf6d24eb-e79f-40df-a89b-c30f13cd1d3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 316,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 8,
                "x": 489,
                "y": 83
            }
        },
        {
            "Key": 317,
            "Value": {
                "id": "07380376-ea30-4180-bb4e-b78efaf8a38b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 317,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 475,
                "y": 83
            }
        },
        {
            "Key": 318,
            "Value": {
                "id": "34c096a9-760c-4d96-b8b9-17d4cdea4ab7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 318,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 460,
                "y": 83
            }
        },
        {
            "Key": 319,
            "Value": {
                "id": "42395ce8-c5c3-4379-aad9-e6644172c956",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 319,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 446,
                "y": 83
            }
        },
        {
            "Key": 320,
            "Value": {
                "id": "f32aaa5d-a986-44cc-b79b-4e14384267fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 320,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 13,
                "x": 431,
                "y": 83
            }
        },
        {
            "Key": 321,
            "Value": {
                "id": "7612accc-1590-4303-a6c9-fcbe4ab264da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 321,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 415,
                "y": 83
            }
        },
        {
            "Key": 322,
            "Value": {
                "id": "dc86fe16-bf2f-4928-b64e-52a0ca91fd20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 322,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 402,
                "y": 83
            }
        },
        {
            "Key": 323,
            "Value": {
                "id": "c3f3e881-41ce-4be9-b7e8-0953f7725693",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 323,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 388,
                "y": 83
            }
        },
        {
            "Key": 324,
            "Value": {
                "id": "9c7dc16b-2938-42a7-b388-3283dde83c8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 324,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 375,
                "y": 83
            }
        },
        {
            "Key": 325,
            "Value": {
                "id": "05b5d38f-25a3-490d-ae0a-c802e3f14a33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 325,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 361,
                "y": 83
            }
        },
        {
            "Key": 326,
            "Value": {
                "id": "2e53a351-c4fc-486e-8bce-b68f187a88db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 326,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 348,
                "y": 83
            }
        },
        {
            "Key": 327,
            "Value": {
                "id": "e1a0ffea-6155-412b-88a7-2e8e838c8938",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 327,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 334,
                "y": 83
            }
        },
        {
            "Key": 328,
            "Value": {
                "id": "6a151803-0a6e-43e0-b0bc-5c7361c73d4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 328,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 321,
                "y": 83
            }
        },
        {
            "Key": 329,
            "Value": {
                "id": "c4ebca2b-1fb3-4011-bac7-d1a87e4b2e7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 329,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 306,
                "y": 83
            }
        },
        {
            "Key": 330,
            "Value": {
                "id": "f2aecd9e-ce4f-4cf2-b20c-0fcf33bb1d19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 330,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 292,
                "y": 83
            }
        },
        {
            "Key": 331,
            "Value": {
                "id": "ad3415b9-54d1-46f0-8268-bbc78958d173",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 331,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 31,
                "y": 110
            }
        },
        {
            "Key": 332,
            "Value": {
                "id": "a50a8f0c-c042-4bbd-88fd-fc8d0db57bc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 332,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 58,
                "y": 110
            }
        },
        {
            "Key": 333,
            "Value": {
                "id": "2c239a79-f47d-450a-8ea2-c3f5de57419f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 333,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 338,
                "y": 110
            }
        },
        {
            "Key": 334,
            "Value": {
                "id": "936c7867-d07e-490c-97fb-09c4a199df79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 334,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 74,
                "y": 110
            }
        },
        {
            "Key": 335,
            "Value": {
                "id": "03479618-8ef4-4566-90b5-6bba31dbf21c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 335,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 323,
                "y": 110
            }
        },
        {
            "Key": 336,
            "Value": {
                "id": "f1bb73f1-a433-44d5-9238-d134e4aa03ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 336,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 307,
                "y": 110
            }
        },
        {
            "Key": 337,
            "Value": {
                "id": "8e448960-cd5a-456e-92f5-d7d7ab8faed4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 337,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 292,
                "y": 110
            }
        },
        {
            "Key": 338,
            "Value": {
                "id": "2fe0e852-6b7a-4771-8694-c810cd416c0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 338,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 275,
                "y": 110
            }
        },
        {
            "Key": 339,
            "Value": {
                "id": "5aa623d0-e417-474d-a67c-ed24360609f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 339,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 258,
                "y": 110
            }
        },
        {
            "Key": 340,
            "Value": {
                "id": "18880d47-c8eb-4555-a816-22aa935f08b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 340,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 13,
                "x": 243,
                "y": 110
            }
        },
        {
            "Key": 341,
            "Value": {
                "id": "42b0c2aa-44aa-4df6-bb51-833baefa02f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 341,
                "h": 25,
                "offset": 3,
                "shift": 14,
                "w": 11,
                "x": 230,
                "y": 110
            }
        },
        {
            "Key": 342,
            "Value": {
                "id": "bf1f31d7-c040-4a39-b6d3-e4c2e70002b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 342,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 13,
                "x": 215,
                "y": 110
            }
        },
        {
            "Key": 343,
            "Value": {
                "id": "40d5a252-ebca-4f7a-aaac-19f280e7ea13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 343,
                "h": 25,
                "offset": 3,
                "shift": 14,
                "w": 11,
                "x": 202,
                "y": 110
            }
        },
        {
            "Key": 344,
            "Value": {
                "id": "966a421d-5d47-4b99-88a5-0d92d06baf8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 344,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 13,
                "x": 187,
                "y": 110
            }
        },
        {
            "Key": 345,
            "Value": {
                "id": "26968503-3f96-473b-a183-c982bfe25991",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 345,
                "h": 25,
                "offset": 3,
                "shift": 14,
                "w": 11,
                "x": 174,
                "y": 110
            }
        },
        {
            "Key": 346,
            "Value": {
                "id": "2526a0e7-e194-4298-a258-02dcf515a85b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 346,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 159,
                "y": 110
            }
        },
        {
            "Key": 347,
            "Value": {
                "id": "bfb0bb7d-7916-4977-8631-7030ccc7ef10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 347,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 146,
                "y": 110
            }
        },
        {
            "Key": 348,
            "Value": {
                "id": "9cce0576-9a81-43a0-8850-364f3a342107",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 348,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 131,
                "y": 110
            }
        },
        {
            "Key": 349,
            "Value": {
                "id": "d0b53f22-02ad-443a-a9e2-7f330e05c968",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 349,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 118,
                "y": 110
            }
        },
        {
            "Key": 350,
            "Value": {
                "id": "d827ceb0-d3bf-4a13-acd6-bc481cb67e90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 350,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 103,
                "y": 110
            }
        },
        {
            "Key": 351,
            "Value": {
                "id": "290918a7-712f-48f8-b7b4-806cb37e6cfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 351,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 90,
                "y": 110
            }
        },
        {
            "Key": 352,
            "Value": {
                "id": "9a777bbf-718e-430c-9c78-a1dd24147f1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 352,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 277,
                "y": 83
            }
        },
        {
            "Key": 353,
            "Value": {
                "id": "fc168e1e-c5fa-4570-8702-184eb8addab2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 353,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 264,
                "y": 83
            }
        },
        {
            "Key": 354,
            "Value": {
                "id": "98300d17-27f0-4b7c-bdca-ef7af156b772",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 354,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 247,
                "y": 83
            }
        },
        {
            "Key": 355,
            "Value": {
                "id": "1b67bf1e-0777-4964-8af9-e6ec2017ab82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 355,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 233,
                "y": 83
            }
        },
        {
            "Key": 356,
            "Value": {
                "id": "58756f3b-0bee-4f91-bab2-dbdc0321a7f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 356,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 392,
                "y": 56
            }
        },
        {
            "Key": 357,
            "Value": {
                "id": "7610fd13-5330-4878-8e30-93a77ad6cd0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 357,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 378,
                "y": 56
            }
        },
        {
            "Key": 358,
            "Value": {
                "id": "9deba73e-3fc4-401b-93f6-f55e553c5a39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 358,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 361,
                "y": 56
            }
        },
        {
            "Key": 359,
            "Value": {
                "id": "bafaf075-c87c-4df5-adc7-8707359e5d1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 359,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 347,
                "y": 56
            }
        },
        {
            "Key": 360,
            "Value": {
                "id": "7f57dd8c-439f-4750-bdb7-f1e8a9371371",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 360,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 333,
                "y": 56
            }
        },
        {
            "Key": 361,
            "Value": {
                "id": "10f47e6e-2595-46f7-a97d-b56d6765c7db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 361,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 320,
                "y": 56
            }
        },
        {
            "Key": 362,
            "Value": {
                "id": "77ee7c45-7754-46bf-abae-02352dc1cdd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 362,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 306,
                "y": 56
            }
        },
        {
            "Key": 363,
            "Value": {
                "id": "408a5eac-2c2c-4e6c-84a6-e16fe93a35cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 363,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 409,
                "y": 56
            }
        },
        {
            "Key": 364,
            "Value": {
                "id": "5fed2b66-291a-4163-9526-993fe2d07b84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 364,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 292,
                "y": 56
            }
        },
        {
            "Key": 365,
            "Value": {
                "id": "8989a25f-b004-4656-bc4b-dc818e7f5df0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 365,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 262,
                "y": 56
            }
        },
        {
            "Key": 366,
            "Value": {
                "id": "8e1348b2-c325-4c05-82a3-afd82f490808",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 366,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 248,
                "y": 56
            }
        },
        {
            "Key": 367,
            "Value": {
                "id": "2d19f038-c215-413f-ace8-75613c32f2e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 367,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 235,
                "y": 56
            }
        },
        {
            "Key": 368,
            "Value": {
                "id": "f916122a-aeae-4fba-bac2-3bfdd0816cbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 368,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 220,
                "y": 56
            }
        },
        {
            "Key": 369,
            "Value": {
                "id": "25afaabf-6fa2-41b1-a509-93a4e0a51c7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 369,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 206,
                "y": 56
            }
        },
        {
            "Key": 370,
            "Value": {
                "id": "a8b7a662-83e3-4b06-a099-227c2f780170",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 370,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 192,
                "y": 56
            }
        },
        {
            "Key": 371,
            "Value": {
                "id": "a3fedd44-3fe9-4354-94bf-634d69814f6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 371,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 179,
                "y": 56
            }
        },
        {
            "Key": 372,
            "Value": {
                "id": "3ce0a2e9-1a44-4cc7-9247-8a90a5a7057d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 372,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 275,
                "y": 56
            }
        },
        {
            "Key": 373,
            "Value": {
                "id": "ab68c480-7d1a-4cac-a656-978378ca15d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 373,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 422,
                "y": 56
            }
        },
        {
            "Key": 374,
            "Value": {
                "id": "f0bfc258-a747-4cf3-b40c-330b43430a25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 374,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 439,
                "y": 56
            }
        },
        {
            "Key": 375,
            "Value": {
                "id": "0fe50155-f69b-40aa-bfef-9e33cb1de232",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 375,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 456,
                "y": 56
            }
        },
        {
            "Key": 376,
            "Value": {
                "id": "a9f7c3b3-e4f5-42ad-a0f6-d1c3a10c0932",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 376,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 216,
                "y": 83
            }
        },
        {
            "Key": 377,
            "Value": {
                "id": "78226b44-090f-4f72-83b6-48e1c96e783f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 377,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 201,
                "y": 83
            }
        },
        {
            "Key": 378,
            "Value": {
                "id": "d94a55c2-b6f8-4447-baa6-7c1cc0dd3b19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 378,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 187,
                "y": 83
            }
        },
        {
            "Key": 379,
            "Value": {
                "id": "26c32fdd-309d-419a-9282-e2dbd0091c3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 379,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 172,
                "y": 83
            }
        },
        {
            "Key": 380,
            "Value": {
                "id": "ce68d6e0-97fa-4e1a-8efd-6f3fee13fa89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 380,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 158,
                "y": 83
            }
        },
        {
            "Key": 381,
            "Value": {
                "id": "a988cd59-babc-4dd4-bfee-96330fe1923b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 381,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 143,
                "y": 83
            }
        },
        {
            "Key": 382,
            "Value": {
                "id": "5fd4600a-b323-4838-ae52-d9a233fd3276",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 382,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 129,
                "y": 83
            }
        },
        {
            "Key": 383,
            "Value": {
                "id": "dbb4de0b-feb4-4e1b-94b2-71e642728b56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 383,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 13,
                "x": 114,
                "y": 83
            }
        },
        {
            "Key": 402,
            "Value": {
                "id": "10794c37-35a7-48f4-b453-ef18c431f46a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 402,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 99,
                "y": 83
            }
        },
        {
            "Key": 506,
            "Value": {
                "id": "d5fbfc44-2ed3-41c0-aec6-8e8802adbd3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 506,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 82,
                "y": 83
            }
        },
        {
            "Key": 507,
            "Value": {
                "id": "f639d484-bdee-491a-a55b-864977958582",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 507,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 67,
                "y": 83
            }
        },
        {
            "Key": 508,
            "Value": {
                "id": "3110a1bf-46cf-4c7b-92a1-add9907c6ec3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 508,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 50,
                "y": 83
            }
        },
        {
            "Key": 509,
            "Value": {
                "id": "05e6e0b7-4fae-44be-8147-62abd37379dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 509,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 33,
                "y": 83
            }
        },
        {
            "Key": 510,
            "Value": {
                "id": "70382923-be9d-475d-97c5-746d55476151",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 510,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 17,
                "y": 83
            }
        },
        {
            "Key": 511,
            "Value": {
                "id": "52973ce4-5e33-4e86-a00b-70b713fb6dd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 511,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 83
            }
        },
        {
            "Key": 536,
            "Value": {
                "id": "8e898a29-f245-4d41-ad2c-9af8216732df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 536,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 484,
                "y": 56
            }
        },
        {
            "Key": 537,
            "Value": {
                "id": "ea64909c-e160-4c0a-8ead-233fc7304547",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 537,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 471,
                "y": 56
            }
        },
        {
            "Key": 538,
            "Value": {
                "id": "4fe441f5-111f-426b-a3f1-17d9b003a6c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 538,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 2,
                "y": 191
            }
        },
        {
            "Key": 539,
            "Value": {
                "id": "fdd45131-d3f6-42ea-802b-4b58007c6f88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 539,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 131,
                "y": 245
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 150,
            "y": 600
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 18,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}