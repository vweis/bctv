{
    "id": "45a7ab9f-ea4d-40f4-ad15-fe22ad1727f0",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_dialogue",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Lucida Console",
    "glyphOperations": 8,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "a842746b-46db-4720-ae15-5eaf97a417de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "3f9ec02a-2ac0-4dcf-b3f8-757eff2c0ade",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 25,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 204,
                "y": 164
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a665ed4b-3532-4a7a-b084-ef8d56b4049c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 192,
                "y": 164
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "46296aab-ac20-4a4c-8d1f-3ee55109fee9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 176,
                "y": 164
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "fecddc30-ce29-41cf-aeb8-a4a0cacc5b99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 163,
                "y": 164
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "b43cf947-8d3d-4f1c-a0ba-52eee1f1bc19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 146,
                "y": 164
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "beb50165-5b41-4406-8aa0-816d824fa2b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 129,
                "y": 164
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "6dbd8d70-4bbe-4157-9b30-f546ca625ae4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 25,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 123,
                "y": 164
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "ee7053bf-92ea-4f57-b47d-7c0b59fcf3ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 25,
                "offset": 4,
                "shift": 14,
                "w": 9,
                "x": 112,
                "y": 164
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "6cbae835-d422-465e-86a1-daa02040ab15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 10,
                "x": 100,
                "y": 164
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "0e697ea3-190c-402c-b3be-4900b6fa29e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 86,
                "y": 164
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "583f5509-4c2c-47da-80d0-55753c326d07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 71,
                "y": 164
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "51c7ecd9-f47c-44fb-abfb-376fcbdde7a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 25,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 65,
                "y": 164
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "5fa4aa51-aabd-41ca-987b-d7d46e726fa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 53,
                "y": 164
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "3f41c18f-9412-494b-ba10-abc4eedb0be7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 25,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 47,
                "y": 164
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "cdb49ffe-46c6-44dd-a6b6-bac702b2d5ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 31,
                "y": 164
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "51b48f8d-512b-4c68-84c9-34567fc3dfa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 16,
                "y": 164
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "9cbb3838-6cb7-417a-a298-3992d250b957",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 164
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "7f99c345-7eb6-4a6f-bf5c-8bbebd6321b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 210,
                "y": 164
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "be1ad75f-8a0e-4c3b-a821-7d3f1ba46491",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 223,
                "y": 164
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "b3b824a4-26ed-4267-aa57-5935364719b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 236,
                "y": 164
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "6574c0f1-de37-4397-bd16-529466fcec73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 25,
                "offset": 3,
                "shift": 14,
                "w": 10,
                "x": 251,
                "y": 164
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "be3d21dd-090b-42cd-bbee-41c56b931961",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 495,
                "y": 164
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f1ac5d9c-5bcb-41bf-abd5-4ad47b9e72cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 482,
                "y": 164
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "947044ee-ac3b-404c-a56a-8ca0d492ea6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 468,
                "y": 164
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "3dc7d2ae-ebc2-460f-b0db-1e039469f46e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 454,
                "y": 164
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "3c6d7a28-0d55-47af-9b56-489c9d554b8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 25,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 448,
                "y": 164
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "d4bbb64e-1640-4cd4-8b9a-85cb804959e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 25,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 442,
                "y": 164
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "13ede877-89d4-4d8c-806a-886fb6aed6ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 427,
                "y": 164
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "a22fede5-7386-401d-b13d-651c0a64af2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 412,
                "y": 164
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d47b60a8-3842-43e3-ba3d-87c33aff9c13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 191
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "f76608af-685b-48ae-b21d-5aa0f5874268",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 398,
                "y": 164
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "3851f651-8c5b-4868-b2e7-b0a35c6bba40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 368,
                "y": 164
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "9698ae84-73d5-4eff-9064-48033805316b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 351,
                "y": 164
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b5302edb-81fc-48c2-850c-d4345e8e8cd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 337,
                "y": 164
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "4645d748-edd9-4667-8eb3-0d8357a13f22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 322,
                "y": 164
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "7ffffaa4-b59e-4028-99a0-294d1b2d4608",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 307,
                "y": 164
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "41ac4b3b-a8a2-4fa3-be43-898d8d425e3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 293,
                "y": 164
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "6c0c98fb-4144-47ca-b4fb-7c9f2774919b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 279,
                "y": 164
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "8a2ddda9-1ac3-4327-a8e4-1dfbed5510e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 263,
                "y": 164
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "2eb0db4a-1cd0-4e3d-8e1a-1c40834d0c5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 491,
                "y": 137
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "af57d435-d148-4cf4-b1d3-3c80ebd3adce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 478,
                "y": 137
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "a6764c52-1cda-4746-89f5-e03bc293aaeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 9,
                "x": 467,
                "y": 137
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "46dd85e6-baa0-4ef3-bfc9-8ac6a77b8783",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 13,
                "x": 180,
                "y": 137
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "a50240a8-4b7a-4173-a33c-fd9eb059be3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 156,
                "y": 137
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "2712eba7-36a0-44e1-b414-a6c1f096d68e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 141,
                "y": 137
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "715a4b99-77e1-4036-9566-dbe907c22b73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 127,
                "y": 137
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "ba1606e8-6a6b-4e9b-8ccb-352638492cd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 111,
                "y": 137
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "1e24298c-655a-488e-9ae2-117fc5c6364f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 97,
                "y": 137
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "950b5d04-894f-4c58-be32-a0c168e5a596",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 80,
                "y": 137
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "78e59343-83a1-4bc6-b8cc-1d71e46c5829",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 13,
                "x": 65,
                "y": 137
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "2cec738a-b9e0-493a-8c44-8745f5a3dd7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 50,
                "y": 137
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "162635f9-7e42-4865-aba8-6d344805eed9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 33,
                "y": 137
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "7c5c99f3-fd2c-425b-b8ec-24dc5e739da8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 19,
                "y": 137
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "0ccc4bc1-507f-483c-b312-e189a1a008b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 2,
                "y": 137
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "1cdc1fa6-cbca-4e5b-a215-56d80a5d705c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 485,
                "y": 110
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "cf9623c2-1381-48a7-9eca-60f8baffa864",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 468,
                "y": 110
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "daf393d7-3224-464e-a20b-eee4feb35086",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 451,
                "y": 110
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "89b62c0f-e9cb-4d5d-9679-cf0961ac7731",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 436,
                "y": 110
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "77e8fd37-692f-4951-9fdf-a5ab5fe9f2d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 25,
                "offset": 5,
                "shift": 14,
                "w": 8,
                "x": 426,
                "y": 110
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "cd93a9be-20ac-4939-a2ca-fc146ddf4fb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 410,
                "y": 110
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "29ea4d35-86dd-448a-b884-75b34f205ec0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 8,
                "x": 170,
                "y": 137
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "e82fadfd-87dc-4202-a09b-37a763356eef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 195,
                "y": 137
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "019e2047-f935-444c-a1a1-cf363771eb60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 450,
                "y": 137
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "99a48e0a-1ecf-48af-9143-3803103e9f09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 25,
                "offset": 4,
                "shift": 14,
                "w": 7,
                "x": 210,
                "y": 137
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "de5397c5-0790-4cc4-b19b-0d579cfbedc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 435,
                "y": 137
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "9fb75c12-cde9-4cdc-9210-bb510b136795",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 421,
                "y": 137
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "d7d1e20e-617b-47a4-8924-27fda2443115",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 407,
                "y": 137
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "7e559d9f-c959-4050-a013-7cfe7d5094c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 393,
                "y": 137
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "2337e3ba-64c2-42a2-86be-c8202a385547",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 379,
                "y": 137
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "3105223f-5b66-4f0c-b8e5-9db1ef0018e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 14,
                "x": 363,
                "y": 137
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "a22c3f6e-72a6-4593-9835-a58f5501367a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 349,
                "y": 137
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "ae3b2249-2621-4d83-ab37-3934b22d4a70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 336,
                "y": 137
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "d065abc0-5511-4c28-ad21-5eb8b1a4e2e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 8,
                "x": 326,
                "y": 137
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "a1de08ea-8e34-48e9-a626-434f8f7dac0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 10,
                "x": 314,
                "y": 137
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "c5472999-282f-4c8c-9a4e-6a876c1ad4b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 300,
                "y": 137
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "9d1c9d5d-840b-4ea5-83ab-a978016f8c3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 8,
                "x": 290,
                "y": 137
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c7b7cf9e-a0df-455a-aa57-ad6d5b81bd81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 275,
                "y": 137
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "12fdfe68-8625-462f-98ea-dc55e1a40258",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 262,
                "y": 137
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "079e9b0b-1fd3-4ff6-b62f-83b06abf609c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 247,
                "y": 137
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "32d35d8f-4591-4713-80e5-e00657331af1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 233,
                "y": 137
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "915801f5-08c5-496d-8224-6c2f09316a68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 219,
                "y": 137
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "266fac2e-9df3-4450-9d0e-6398f8508426",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 25,
                "offset": 3,
                "shift": 14,
                "w": 11,
                "x": 385,
                "y": 164
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "b1d97a4a-e1f8-4c13-a40f-9125a1d14510",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 17,
                "y": 191
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "57d5ac80-b139-4716-a98a-2eec610b4f91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 30,
                "y": 191
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "bf9b377b-bfef-4a81-8b56-f04401d4b48a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 44,
                "y": 191
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "e2f0c6e8-04cb-4c4b-aa5c-deb71774b5ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 403,
                "y": 218
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "56851496-0461-40a6-9f25-a97eb5ab3886",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 386,
                "y": 218
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "013ae5cd-0a72-4fb3-a787-3723e7673145",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 371,
                "y": 218
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "27b06e65-6dfb-4359-92d7-982efca0d643",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 356,
                "y": 218
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "ee01af89-fdd5-49bb-857c-0d5a435a8de1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 342,
                "y": 218
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "becf6535-01ab-4975-982e-458e3a13705c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 330,
                "y": 218
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "0d95a031-57c7-4f1c-8097-560b903c99d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 25,
                "offset": 6,
                "shift": 14,
                "w": 3,
                "x": 325,
                "y": 218
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "7a70a0fa-5dd7-4920-9238-51cbd6f1f864",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 313,
                "y": 218
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "44328d53-cf67-4ae2-9fb4-b4712e72aa79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 298,
                "y": 218
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "03e36d9b-f418-4526-b7f7-ff73ef4715e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 0,
                "x": 296,
                "y": 218
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "34fc6ce6-36ab-48dc-bed2-6205ab73235b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 25,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 290,
                "y": 218
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "198642f0-fd6d-4b4e-b710-057b8ac4e4ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 278,
                "y": 218
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "6fb7ef2f-4785-4cda-8e90-6ff0bf6e82a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 265,
                "y": 218
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "bcc6fb52-4d29-4ea5-a9e6-62f2fe1ee2d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 250,
                "y": 218
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "457e3663-e6af-4ccf-b59e-d42fe3da7c53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 233,
                "y": 218
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "d562ea4a-66bf-4f9a-b77e-3179ba2e323c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 25,
                "offset": 6,
                "shift": 14,
                "w": 3,
                "x": 228,
                "y": 218
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "30945f05-37f2-4969-9930-f16301dcd30e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 215,
                "y": 218
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "2721f8fb-8bc2-4d81-bfa1-cf00287ff312",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 25,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 419,
                "y": 218
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "b1de1d6d-7db7-4ecd-8b5a-db7b5e0a478d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 430,
                "y": 218
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "a2b222af-86b1-4b9d-a1e3-3129227e15e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 446,
                "y": 218
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "8347add6-2f16-4598-8f86-90aa9732c659",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 459,
                "y": 218
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "75556dd6-3544-4c44-ba8d-7af04d4b3ac3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 177,
                "y": 245
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "1db0979f-1672-496d-9870-3983719aeee2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 165,
                "y": 245
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "98f3b5ab-1c72-4b1c-bd71-e17c4380d045",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 152,
                "y": 245
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "3cad8ee9-9674-49b6-86d6-80f40711a9d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 135,
                "y": 245
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "21ec96ac-19d2-4052-b907-58fa986721a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 25,
                "offset": 4,
                "shift": 14,
                "w": 6,
                "x": 127,
                "y": 245
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "928d659f-d372-48f0-beaf-f79e93b93329",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 112,
                "y": 245
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "a5d5f4ad-f0e7-4bd9-8e99-0d0b3ff4eb98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 25,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 101,
                "y": 245
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "dde8ef75-ddf5-46c7-84ff-8d49a41caa19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 25,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 90,
                "y": 245
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "5caeaeb9-8e3b-4cd2-a206-e2a9220187b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 25,
                "offset": 4,
                "shift": 14,
                "w": 7,
                "x": 192,
                "y": 245
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "7380b9f7-5ab2-464d-a9f4-3182001d5425",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 76,
                "y": 245
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "09625030-ec56-4d2e-9533-c418ab3fa51e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 49,
                "y": 245
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "0d80c34b-8e51-48a3-af1b-68af40b6e78a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 25,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 43,
                "y": 245
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "5d3203c1-eddb-430e-8e1a-417865252f60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 25,
                "offset": 5,
                "shift": 14,
                "w": 5,
                "x": 36,
                "y": 245
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "96c20254-46b8-4453-8f08-a623e6dbfeaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 25,
                "offset": 4,
                "shift": 14,
                "w": 5,
                "x": 29,
                "y": 245
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "8b31376f-2039-4fc9-b0a5-73539787ac8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 17,
                "y": 245
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "ed8c18fd-48e0-4caf-b1fa-ff3ae808fe88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 245
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "7b9665cb-ca15-4b06-b8c4-010ea2ca5e55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 490,
                "y": 218
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "b18bff33-46ed-4314-bf37-0040a2eca956",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 473,
                "y": 218
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "9d1d27fc-987d-44fe-b7e8-a7619714e424",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 198,
                "y": 218
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "9ec8559f-1c15-4522-b30f-c52d5c0a60cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 62,
                "y": 245
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "e3427987-9c38-4730-97b5-ea90bf17c807",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 181,
                "y": 218
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "37092353-3f0d-4816-8d13-15d37a759e8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 2,
                "y": 218
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "811d08aa-a67c-44f9-8b42-6c8be4cced08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 294,
                "y": 191
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "b3cf97ee-c6ae-4e68-8c8b-cf9247f2c596",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 277,
                "y": 191
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "ab8bd72a-c07d-445c-a500-a4891640790b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 260,
                "y": 191
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "76bd43c2-efd3-4821-889a-3e6154dc1bb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 243,
                "y": 191
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "87de7d5b-a45c-416c-9576-cfb5a9a81a60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 226,
                "y": 191
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "c8559933-ec54-4f20-a6b6-84900acfc1cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 211,
                "y": 191
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "356f0d30-ccc6-45ce-9161-dc391ad92997",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 197,
                "y": 191
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "4eda03e7-758e-424e-ba75-00319f0dd6ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 183,
                "y": 191
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "feea025c-87dc-4126-88f3-eba529f417c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 169,
                "y": 191
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "1d8ad26b-08ce-445a-872c-008c90e828a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 155,
                "y": 191
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "600a8aee-a7b1-4319-90a4-f76bbeeda3a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 142,
                "y": 191
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "37b6a07d-4b15-418e-86f9-a77db992aa76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 129,
                "y": 191
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "13caac45-b59f-4a25-84d8-3f6774d9fb63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 116,
                "y": 191
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "f056f695-f1a6-46c2-97b7-9af85f866169",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 103,
                "y": 191
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "c50142d7-56c1-4f3b-ab3a-e0acbe4c9212",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 87,
                "y": 191
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "1fd84d04-67ed-4e97-9c23-d998c88dcc31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 73,
                "y": 191
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "6990bbf0-28dc-4b64-abf8-47c31d264818",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 57,
                "y": 191
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "8900735f-5624-4e4c-b043-2e3018bcbfb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 311,
                "y": 191
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "298178f1-47e5-4dae-a511-6f2217d9384c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 327,
                "y": 191
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "941b352e-eb29-49a8-9ef9-6d720cef0ce6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 343,
                "y": 191
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "d4b77bd9-0330-4a76-9b7a-a0eb67a764e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 359,
                "y": 191
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "bfbc9ad4-b950-40ed-b3ba-c6102e44e646",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 137,
                "y": 218
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "c218452f-3c52-4739-8600-f2662a91861c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 121,
                "y": 218
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "764f9f38-ab00-401d-a4f3-96680442b68c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 107,
                "y": 218
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "b7387dfb-bb69-43e9-950c-e66e70daeb71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 93,
                "y": 218
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "ce989458-9618-460f-b94b-ae6f78b0f5ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 79,
                "y": 218
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "4c2b0fbd-e389-4087-9299-95663d943d5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 65,
                "y": 218
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "f7ac6890-91a6-4c8a-8e12-5f8756651568",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 48,
                "y": 218
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "8400bfab-53ba-487a-9d59-8043c7ae4502",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 34,
                "y": 218
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "73f23d94-cc53-42b8-b2eb-890e92f50775",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 153,
                "y": 218
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "edfec866-0fb4-400f-a743-348ee4dfe47d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 19,
                "y": 218
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "c0e51ecb-ca5e-4ca6-bab8-ebb47f123fdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 480,
                "y": 191
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "a37fb7dd-b562-422e-97a0-db5ccef808a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 465,
                "y": 191
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "a67bba4b-00d1-4884-85bb-ec8cbb0f821b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 450,
                "y": 191
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "681d3b35-1500-42b7-b909-dc172b194ece",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 435,
                "y": 191
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "4c66e194-e5a9-4954-851a-8e17952509ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 420,
                "y": 191
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "177a4a00-ef56-4034-9121-dbe78e050a40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 403,
                "y": 191
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "3aa93e2f-ac2d-4731-b86b-1828086595a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 389,
                "y": 191
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "5f549fca-33a5-4b4e-beb2-507dc2f8032a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 375,
                "y": 191
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "a347c13e-3e2f-4b2e-9d10-2784b06bee9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 396,
                "y": 110
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "d07b4d00-447e-4412-996b-b905167c7540",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 167,
                "y": 218
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "51ada110-d31c-44d4-a22a-692eb31c44a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 382,
                "y": 110
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "332ff757-ce89-4998-a788-0c0334ee6d0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 8,
                "x": 259,
                "y": 83
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "e3b704de-f5c0-4e8a-a76b-b6b2a269c5f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 314,
                "y": 29
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "1f1fad19-285d-4f9e-8680-101927d9ce39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 302,
                "y": 29
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "379ff17a-b8ea-4796-8b53-3248e8dc8af5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 9,
                "x": 291,
                "y": 29
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "9d3b4ca5-5b13-4bf4-932a-8d4fbdfd3699",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 276,
                "y": 29
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "7ce54778-27a1-411a-bfcb-2b3b331150f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 263,
                "y": 29
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "298c9942-9827-4737-ad90-35383c6381f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 248,
                "y": 29
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "d293ec9c-8cf0-4f45-982d-e2d6666336a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 233,
                "y": 29
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "a3d31fbc-af12-4cc9-94d5-85efca97a374",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 218,
                "y": 29
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "d2d7510b-1b1a-4696-9e4a-16a187f88388",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 203,
                "y": 29
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "55bd7dc1-a82d-4786-9567-d58ac0b5f64e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 188,
                "y": 29
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "726c2deb-a352-433f-9ea6-2e6bc3d4cda6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 173,
                "y": 29
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "3d8acedd-c3da-4652-bf8f-111aee3d259d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 158,
                "y": 29
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "b06a4328-528f-4ccf-8b49-935d7fedf15a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 145,
                "y": 29
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "3d91835b-523d-4339-97f3-5ff11d54fb08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 132,
                "y": 29
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "98940eb4-661e-45b5-9a45-ec5db236a167",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 119,
                "y": 29
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "cca402f0-bfc5-4a5e-8ab1-f699b1b44187",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 106,
                "y": 29
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "67547497-1594-4cf3-92dd-6c3eb11e3538",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 91,
                "y": 29
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "6b8a1924-17fb-44e6-942c-e7baf6336d82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 326,
                "y": 29
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "c0c7e9cd-5a57-444b-bb75-90924346d7c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 340,
                "y": 29
            }
        },
        {
            "Key": 256,
            "Value": {
                "id": "2e722be2-71ec-4e59-a2a7-487171de6554",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 256,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 355,
                "y": 29
            }
        },
        {
            "Key": 257,
            "Value": {
                "id": "c4ab0771-b5dd-473c-a93c-64ed6717fc30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 257,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 372,
                "y": 29
            }
        },
        {
            "Key": 258,
            "Value": {
                "id": "fb1c5dbd-cc59-4157-b7e5-0a989be6d9a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 258,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 136,
                "y": 56
            }
        },
        {
            "Key": 259,
            "Value": {
                "id": "05e2b9af-e99c-40f5-94c3-b4f221c7ff92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 259,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 121,
                "y": 56
            }
        },
        {
            "Key": 260,
            "Value": {
                "id": "05d18556-1b61-4b45-b53e-47bf17c6c98e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 260,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 104,
                "y": 56
            }
        },
        {
            "Key": 261,
            "Value": {
                "id": "c8ce3898-f66a-4bf6-9daf-770babd336c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 261,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 89,
                "y": 56
            }
        },
        {
            "Key": 262,
            "Value": {
                "id": "a240c704-80eb-477c-922c-b55cc143f870",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 262,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 74,
                "y": 56
            }
        },
        {
            "Key": 263,
            "Value": {
                "id": "230d775c-eda2-4a9f-8b67-121093674982",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 263,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 60,
                "y": 56
            }
        },
        {
            "Key": 264,
            "Value": {
                "id": "9e8c3d07-71a5-4bfd-be22-8105e9c31094",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 264,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 45,
                "y": 56
            }
        },
        {
            "Key": 265,
            "Value": {
                "id": "04ab50d7-d067-4f26-88b0-fe3ae8f58c07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 265,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 31,
                "y": 56
            }
        },
        {
            "Key": 266,
            "Value": {
                "id": "2b72847b-dfc9-4d67-9c2d-d1d5a607e815",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 266,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 153,
                "y": 56
            }
        },
        {
            "Key": 267,
            "Value": {
                "id": "88fb26cb-f67a-446f-9536-95ed3a2aeb31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 267,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 17,
                "y": 56
            }
        },
        {
            "Key": 268,
            "Value": {
                "id": "b8877e4f-3bb0-4769-954f-33b58e4a1d4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 268,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 491,
                "y": 29
            }
        },
        {
            "Key": 269,
            "Value": {
                "id": "dc09381d-9964-4a7c-b503-c80c261c5dc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 269,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 477,
                "y": 29
            }
        },
        {
            "Key": 270,
            "Value": {
                "id": "ba4113fb-3099-49e2-869a-dd791a24bd90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 270,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 462,
                "y": 29
            }
        },
        {
            "Key": 271,
            "Value": {
                "id": "ce3a0635-b26d-4984-9f25-19888bbb053e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 271,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 447,
                "y": 29
            }
        },
        {
            "Key": 272,
            "Value": {
                "id": "05df3c6a-acaf-41a9-849a-2cb3b943e63d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 272,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 431,
                "y": 29
            }
        },
        {
            "Key": 273,
            "Value": {
                "id": "bd04ea9a-9e65-4681-8cf9-d17952a9fe79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 273,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 14,
                "x": 415,
                "y": 29
            }
        },
        {
            "Key": 274,
            "Value": {
                "id": "71e0d3cf-7888-4742-814b-05eb251fa130",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 274,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 401,
                "y": 29
            }
        },
        {
            "Key": 275,
            "Value": {
                "id": "7ce8ac09-dd80-4335-9079-40cf2fbcde5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 275,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 387,
                "y": 29
            }
        },
        {
            "Key": 276,
            "Value": {
                "id": "3f7780f6-762f-4874-a47f-259f2bffac4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 276,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 77,
                "y": 29
            }
        },
        {
            "Key": 277,
            "Value": {
                "id": "456e4af0-b824-43a2-9d15-d3e4a682896e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 277,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 63,
                "y": 29
            }
        },
        {
            "Key": 278,
            "Value": {
                "id": "86ad65b7-b241-4576-a97d-cd1532d863e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 278,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 49,
                "y": 29
            }
        },
        {
            "Key": 279,
            "Value": {
                "id": "27d32511-8919-44e8-87cf-27b90a0cb1e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 279,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 277,
                "y": 2
            }
        },
        {
            "Key": 280,
            "Value": {
                "id": "046a2695-d1f8-4243-bbef-cd9532756be8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 280,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 253,
                "y": 2
            }
        },
        {
            "Key": 281,
            "Value": {
                "id": "ff577676-2c76-497b-b58c-610ecd32ea02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 281,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 239,
                "y": 2
            }
        },
        {
            "Key": 282,
            "Value": {
                "id": "a27d6776-d901-4e8e-8689-76ed1c5d89d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 282,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 225,
                "y": 2
            }
        },
        {
            "Key": 283,
            "Value": {
                "id": "69b0cdf7-aeef-49eb-b028-e355a3c9c513",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 283,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 284,
            "Value": {
                "id": "27eb0f41-957c-4df5-a555-b6816339b41f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 284,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 195,
                "y": 2
            }
        },
        {
            "Key": 285,
            "Value": {
                "id": "2d0803c5-9945-4314-bd7b-15729cb54a7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 285,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 181,
                "y": 2
            }
        },
        {
            "Key": 286,
            "Value": {
                "id": "b1aac998-7753-4f61-ac13-19c3f3a007e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 286,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 165,
                "y": 2
            }
        },
        {
            "Key": 287,
            "Value": {
                "id": "50e6b84d-aea7-4aa5-b3f8-d73d29f50202",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 287,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 288,
            "Value": {
                "id": "736dc32f-80a8-4b84-afc1-6d288a5e1030",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 288,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 289,
            "Value": {
                "id": "190e435b-a77d-4df2-b7b8-3a57281de32f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 289,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 121,
                "y": 2
            }
        },
        {
            "Key": 290,
            "Value": {
                "id": "6191c092-8aba-48fb-8031-a65cbb03df74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 290,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 105,
                "y": 2
            }
        },
        {
            "Key": 291,
            "Value": {
                "id": "e05ce071-0a12-4266-9411-0699d7b220d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 291,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 292,
            "Value": {
                "id": "51126d48-e303-4754-94e1-bdfd76c0c85f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 292,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 293,
            "Value": {
                "id": "d524989a-c727-4d67-86e7-622e72537372",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 293,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 63,
                "y": 2
            }
        },
        {
            "Key": 294,
            "Value": {
                "id": "17107a48-4a4f-4f9f-b80d-4627d55560f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 294,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 295,
            "Value": {
                "id": "eaa9d277-fb33-4ad4-bde6-f9e8cd81610f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 295,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 296,
            "Value": {
                "id": "1aeb3881-823b-4c7e-a412-cd29a2951adb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 296,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 297,
            "Value": {
                "id": "046acf7d-9f28-4a4f-a023-702c7949259c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 297,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 8,
                "x": 267,
                "y": 2
            }
        },
        {
            "Key": 298,
            "Value": {
                "id": "1810ae01-ea8a-4738-9fab-0a0bb2b231a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 298,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 291,
                "y": 2
            }
        },
        {
            "Key": 299,
            "Value": {
                "id": "d1ef6bbf-f8ed-454b-ba5b-feb671b0633d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 299,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 37,
                "y": 29
            }
        },
        {
            "Key": 300,
            "Value": {
                "id": "201f1fe6-9e30-4003-b978-0c49fa2f4f51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 300,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 304,
                "y": 2
            }
        },
        {
            "Key": 301,
            "Value": {
                "id": "3ceb2dd1-f224-4b74-ace4-0d815b7fb6f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 301,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 25,
                "y": 29
            }
        },
        {
            "Key": 302,
            "Value": {
                "id": "d7bdc4bd-6e55-4b92-bd37-89dba5c404d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 302,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 12,
                "y": 29
            }
        },
        {
            "Key": 303,
            "Value": {
                "id": "a7b52010-8377-4290-b39e-20a2350b4e4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 303,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 8,
                "x": 2,
                "y": 29
            }
        },
        {
            "Key": 304,
            "Value": {
                "id": "dad67cea-2607-472a-920c-4e5c96f2816c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 304,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 490,
                "y": 2
            }
        },
        {
            "Key": 305,
            "Value": {
                "id": "1e04b931-b962-4da7-8ed8-8f65e2bcb12a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 305,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 7,
                "x": 481,
                "y": 2
            }
        },
        {
            "Key": 306,
            "Value": {
                "id": "d74ac900-0626-449e-a261-0ea87198d43e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 306,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 465,
                "y": 2
            }
        },
        {
            "Key": 307,
            "Value": {
                "id": "f0e4d690-b300-4f18-b641-5b51732ada29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 307,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 450,
                "y": 2
            }
        },
        {
            "Key": 308,
            "Value": {
                "id": "c9a07fb3-e090-4b62-915c-64da965611ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 308,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 438,
                "y": 2
            }
        },
        {
            "Key": 309,
            "Value": {
                "id": "6d7eed6f-a000-4a44-ac88-e0cbbf4f8c5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 309,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 10,
                "x": 426,
                "y": 2
            }
        },
        {
            "Key": 310,
            "Value": {
                "id": "5a96d6b2-ac17-4998-9d89-773105cab85c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 310,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 13,
                "x": 411,
                "y": 2
            }
        },
        {
            "Key": 311,
            "Value": {
                "id": "038c0532-b16e-471a-a152-e87f53c01f92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 311,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 397,
                "y": 2
            }
        },
        {
            "Key": 312,
            "Value": {
                "id": "15706c39-f82e-4c06-bf8c-513b655f01ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 312,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 384,
                "y": 2
            }
        },
        {
            "Key": 313,
            "Value": {
                "id": "356270cc-e93d-4f7a-bffe-104fadc7d9ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 313,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 370,
                "y": 2
            }
        },
        {
            "Key": 314,
            "Value": {
                "id": "e3fc07a2-b2af-435e-990f-e868e855cb73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 314,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 13,
                "x": 355,
                "y": 2
            }
        },
        {
            "Key": 315,
            "Value": {
                "id": "d5f2e827-868a-4b19-8486-6b5f918eb96a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 315,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 341,
                "y": 2
            }
        },
        {
            "Key": 316,
            "Value": {
                "id": "8752ac5d-bd2e-4ebd-9070-55154162a2ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 316,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 8,
                "x": 331,
                "y": 2
            }
        },
        {
            "Key": 317,
            "Value": {
                "id": "677bd627-8601-4454-9421-fb9d9e69d4cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 317,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 317,
                "y": 2
            }
        },
        {
            "Key": 318,
            "Value": {
                "id": "f7e1ae99-502e-4aa0-bfd3-5fe3b994812b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 318,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 319,
            "Value": {
                "id": "aa4d5bac-11d4-4064-8b08-044e94916f04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 319,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 168,
                "y": 56
            }
        },
        {
            "Key": 320,
            "Value": {
                "id": "e02b0b27-2586-48d9-a2ea-7f06a014f472",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 320,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 13,
                "x": 182,
                "y": 56
            }
        },
        {
            "Key": 321,
            "Value": {
                "id": "37c4948a-9cb7-4a3a-aeae-924a410eaf7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 321,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 197,
                "y": 56
            }
        },
        {
            "Key": 322,
            "Value": {
                "id": "6f544a29-b271-42d6-bf5b-64aaf84a1e46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 322,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 29,
                "y": 110
            }
        },
        {
            "Key": 323,
            "Value": {
                "id": "5c11ee83-1a46-4895-99a2-09dca57bca8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 323,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 15,
                "y": 110
            }
        },
        {
            "Key": 324,
            "Value": {
                "id": "ed8bd55e-6bbb-4fd6-881d-515ed88f320c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 324,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 2,
                "y": 110
            }
        },
        {
            "Key": 325,
            "Value": {
                "id": "253783b5-6c3f-43af-b1e6-a94b34ad14a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 325,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 487,
                "y": 83
            }
        },
        {
            "Key": 326,
            "Value": {
                "id": "cb34f6bb-3912-4b21-ad4e-f6408aca48ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 326,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 474,
                "y": 83
            }
        },
        {
            "Key": 327,
            "Value": {
                "id": "03ba71b0-ce59-4a2a-b547-5f5d774ddf48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 327,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 460,
                "y": 83
            }
        },
        {
            "Key": 328,
            "Value": {
                "id": "839f7fde-3e10-4162-80a5-8613a3ba10dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 328,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 447,
                "y": 83
            }
        },
        {
            "Key": 329,
            "Value": {
                "id": "4f7478c2-d652-4bde-8218-62ca009d0ccd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 329,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 432,
                "y": 83
            }
        },
        {
            "Key": 330,
            "Value": {
                "id": "102cf56e-93f2-487d-8eae-b0c84998ff8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 330,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 418,
                "y": 83
            }
        },
        {
            "Key": 331,
            "Value": {
                "id": "8fcf78ed-4b58-44b8-83c0-2407bc74f1dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 331,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 405,
                "y": 83
            }
        },
        {
            "Key": 332,
            "Value": {
                "id": "a95b5649-1924-4372-8dcc-0325269c6f49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 332,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 389,
                "y": 83
            }
        },
        {
            "Key": 333,
            "Value": {
                "id": "4d23da96-e51a-4fc3-ae41-b67410a791e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 333,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 374,
                "y": 83
            }
        },
        {
            "Key": 334,
            "Value": {
                "id": "a6cb1a95-8809-425c-848f-0920f11ac495",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 334,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 358,
                "y": 83
            }
        },
        {
            "Key": 335,
            "Value": {
                "id": "afd1c7c6-6467-4287-8172-5ee1fb88ccd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 335,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 343,
                "y": 83
            }
        },
        {
            "Key": 336,
            "Value": {
                "id": "3e07af7a-9219-4447-ae55-014bdabe91c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 336,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 327,
                "y": 83
            }
        },
        {
            "Key": 337,
            "Value": {
                "id": "b12d811b-cdd7-4d4b-a06d-882316c48aad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 337,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 312,
                "y": 83
            }
        },
        {
            "Key": 338,
            "Value": {
                "id": "53c7443b-8655-4c4c-9b00-47758e812b15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 338,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 295,
                "y": 83
            }
        },
        {
            "Key": 339,
            "Value": {
                "id": "0dc80866-369f-452d-9582-799ee5fac47f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 339,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 42,
                "y": 110
            }
        },
        {
            "Key": 340,
            "Value": {
                "id": "57db0740-a535-4187-8179-4309147029e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 340,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 13,
                "x": 59,
                "y": 110
            }
        },
        {
            "Key": 341,
            "Value": {
                "id": "c173bc32-47cc-437e-9631-01458b23fd2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 341,
                "h": 25,
                "offset": 3,
                "shift": 14,
                "w": 11,
                "x": 74,
                "y": 110
            }
        },
        {
            "Key": 342,
            "Value": {
                "id": "2b3bcb6c-2edd-42f4-97e7-31b7b44828d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 342,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 13,
                "x": 87,
                "y": 110
            }
        },
        {
            "Key": 343,
            "Value": {
                "id": "baa7034c-e411-4fe9-8ad3-488cbc1dfe3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 343,
                "h": 25,
                "offset": 3,
                "shift": 14,
                "w": 11,
                "x": 350,
                "y": 110
            }
        },
        {
            "Key": 344,
            "Value": {
                "id": "05516845-a5b2-4b89-b0eb-34529aab75a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 344,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 13,
                "x": 335,
                "y": 110
            }
        },
        {
            "Key": 345,
            "Value": {
                "id": "940924e9-6095-4b0b-a14c-7a0e23a216d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 345,
                "h": 25,
                "offset": 3,
                "shift": 14,
                "w": 11,
                "x": 322,
                "y": 110
            }
        },
        {
            "Key": 346,
            "Value": {
                "id": "a0d7d03b-2f41-49d9-a7e4-7c12ae142f37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 346,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 307,
                "y": 110
            }
        },
        {
            "Key": 347,
            "Value": {
                "id": "685e0a11-5baf-4551-963e-076e916142bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 347,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 294,
                "y": 110
            }
        },
        {
            "Key": 348,
            "Value": {
                "id": "3c535efa-e9ac-40a9-815f-2d82bbf37200",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 348,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 279,
                "y": 110
            }
        },
        {
            "Key": 349,
            "Value": {
                "id": "84a81335-aaa9-4723-bb72-5c28d0bd2feb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 349,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 266,
                "y": 110
            }
        },
        {
            "Key": 350,
            "Value": {
                "id": "e1ec8d8a-3502-4389-9853-846679da370a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 350,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 251,
                "y": 110
            }
        },
        {
            "Key": 351,
            "Value": {
                "id": "71fafc9a-070a-4180-b2b1-c4891bacd10f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 351,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 363,
                "y": 110
            }
        },
        {
            "Key": 352,
            "Value": {
                "id": "3c8300bc-5167-4cfc-82ff-ca4470f91e3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 352,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 236,
                "y": 110
            }
        },
        {
            "Key": 353,
            "Value": {
                "id": "fd9100df-2376-49c6-9573-ad95cd6243ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 353,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 209,
                "y": 110
            }
        },
        {
            "Key": 354,
            "Value": {
                "id": "c8c4c060-af69-4178-9ee1-3d8325dbc824",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 354,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 192,
                "y": 110
            }
        },
        {
            "Key": 355,
            "Value": {
                "id": "4a65e423-bc00-41b3-8951-a51d25446f3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 355,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 178,
                "y": 110
            }
        },
        {
            "Key": 356,
            "Value": {
                "id": "1ce2b627-29ac-4eaf-8961-43f72f7a060f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 356,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 161,
                "y": 110
            }
        },
        {
            "Key": 357,
            "Value": {
                "id": "427f241e-cfc8-4320-b18e-b4efdfbeea59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 357,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 147,
                "y": 110
            }
        },
        {
            "Key": 358,
            "Value": {
                "id": "70e407e2-d555-445f-b2d2-439563446331",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 358,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 130,
                "y": 110
            }
        },
        {
            "Key": 359,
            "Value": {
                "id": "9dcd8c92-b216-48e3-ae5b-57b9ed33a8a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 359,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 116,
                "y": 110
            }
        },
        {
            "Key": 360,
            "Value": {
                "id": "b3bd470b-5b3c-4e75-80f9-ad1293d22d30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 360,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 102,
                "y": 110
            }
        },
        {
            "Key": 361,
            "Value": {
                "id": "7e0fc5b8-d439-4812-ac5b-68fe64d55153",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 361,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 282,
                "y": 83
            }
        },
        {
            "Key": 362,
            "Value": {
                "id": "8b2bb9eb-334c-4dc1-a050-1f283ea78cc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 362,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 222,
                "y": 110
            }
        },
        {
            "Key": 363,
            "Value": {
                "id": "e9387c36-3381-46ad-8606-6e9e37870e96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 363,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 269,
                "y": 83
            }
        },
        {
            "Key": 364,
            "Value": {
                "id": "cda436d9-f78a-4425-a43e-0cba41f27bb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 364,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 95,
                "y": 83
            }
        },
        {
            "Key": 365,
            "Value": {
                "id": "8247c67a-8e09-4c27-b8bc-ae1d218ed90d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 365,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 452,
                "y": 56
            }
        },
        {
            "Key": 366,
            "Value": {
                "id": "ab35d777-7b69-4b91-be03-9de338fe08b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 366,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 438,
                "y": 56
            }
        },
        {
            "Key": 367,
            "Value": {
                "id": "9fc6e4f8-9409-4b58-985b-3dd11d6b4dda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 367,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 425,
                "y": 56
            }
        },
        {
            "Key": 368,
            "Value": {
                "id": "368c0715-87b7-474e-97a7-6ae444637bd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 368,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 410,
                "y": 56
            }
        },
        {
            "Key": 369,
            "Value": {
                "id": "31957610-b8ea-4051-9a5e-6d98538fedc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 369,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 396,
                "y": 56
            }
        },
        {
            "Key": 370,
            "Value": {
                "id": "574c7a5d-146a-4305-b1d1-53bf8d619ee9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 370,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 382,
                "y": 56
            }
        },
        {
            "Key": 371,
            "Value": {
                "id": "3c2ba711-ed0b-4733-8660-e4005739d8b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 371,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 369,
                "y": 56
            }
        },
        {
            "Key": 372,
            "Value": {
                "id": "632847ae-f0d3-44c4-b17c-99ab9fddbaf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 372,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 352,
                "y": 56
            }
        },
        {
            "Key": 373,
            "Value": {
                "id": "b3e0e0da-023f-41b2-b49b-82010155b032",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 373,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 335,
                "y": 56
            }
        },
        {
            "Key": 374,
            "Value": {
                "id": "471754ee-624a-4e2f-9073-15275bc8a992",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 374,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 318,
                "y": 56
            }
        },
        {
            "Key": 375,
            "Value": {
                "id": "4b6756ad-6705-40f9-8b1e-a68d69c89e98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 375,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 303,
                "y": 56
            }
        },
        {
            "Key": 376,
            "Value": {
                "id": "3f4bd1bf-d4bc-4e71-9ac0-f417672ba12d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 376,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 286,
                "y": 56
            }
        },
        {
            "Key": 377,
            "Value": {
                "id": "8915cc1d-014b-4cb8-81b9-0a4e82115396",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 377,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 271,
                "y": 56
            }
        },
        {
            "Key": 378,
            "Value": {
                "id": "2dc64807-30ef-4263-9799-019a0899e8ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 378,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 257,
                "y": 56
            }
        },
        {
            "Key": 379,
            "Value": {
                "id": "6431a0d0-f781-413b-ac20-aed16575436e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 379,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 242,
                "y": 56
            }
        },
        {
            "Key": 380,
            "Value": {
                "id": "5be17f1c-0c9e-47bb-9949-6aae2a04edfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 380,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 228,
                "y": 56
            }
        },
        {
            "Key": 381,
            "Value": {
                "id": "16679835-6c9f-4db8-a87b-346535b29026",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 381,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 213,
                "y": 56
            }
        },
        {
            "Key": 382,
            "Value": {
                "id": "de65a55c-451a-49a4-aac4-139ee37e5baa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 382,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 465,
                "y": 56
            }
        },
        {
            "Key": 383,
            "Value": {
                "id": "2736a6db-5c37-49e6-b7de-aa5e96eed4e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 383,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 13,
                "x": 479,
                "y": 56
            }
        },
        {
            "Key": 402,
            "Value": {
                "id": "8ae6719c-ab6c-4bf7-ae6d-275da54e1214",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 402,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 494,
                "y": 56
            }
        },
        {
            "Key": 506,
            "Value": {
                "id": "3f594630-eec2-4b78-b3fa-df5606ef79cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 506,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 2,
                "y": 83
            }
        },
        {
            "Key": 507,
            "Value": {
                "id": "c70b5f41-a95f-4951-9784-74550336bb9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 507,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 230,
                "y": 83
            }
        },
        {
            "Key": 508,
            "Value": {
                "id": "8fe64abb-3881-4690-b0ff-da3ce06bc6fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 508,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 213,
                "y": 83
            }
        },
        {
            "Key": 509,
            "Value": {
                "id": "b20f9df1-4150-4837-8b6e-06ac6b46d780",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 509,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 196,
                "y": 83
            }
        },
        {
            "Key": 510,
            "Value": {
                "id": "3fa2516d-7a4a-4fbc-8f90-17727642ea29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 510,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 180,
                "y": 83
            }
        },
        {
            "Key": 511,
            "Value": {
                "id": "d2f4a490-bb94-4d0d-935d-073f388e35f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 511,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 165,
                "y": 83
            }
        },
        {
            "Key": 536,
            "Value": {
                "id": "44d6db6a-a33a-4578-8d3f-149c96734965",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 536,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 150,
                "y": 83
            }
        },
        {
            "Key": 537,
            "Value": {
                "id": "972f7b9d-272d-4165-94f0-edc579ec262b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 537,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 137,
                "y": 83
            }
        },
        {
            "Key": 538,
            "Value": {
                "id": "e5f3729d-2a7a-4a36-9fe6-55659f65c64e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 538,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 120,
                "y": 83
            }
        },
        {
            "Key": 539,
            "Value": {
                "id": "190ea16f-b500-4d69-89d2-71d4447bf081",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 539,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 245,
                "y": 83
            }
        },
        {
            "Key": 710,
            "Value": {
                "id": "0deea838-f04f-4367-9028-7d645ae1b839",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 710,
                "h": 25,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 109,
                "y": 83
            }
        },
        {
            "Key": 711,
            "Value": {
                "id": "1cd1ab8e-3cc4-42ca-afc3-9e419310e3fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 711,
                "h": 25,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 84,
                "y": 83
            }
        },
        {
            "Key": 713,
            "Value": {
                "id": "62fa4765-138e-498d-8a99-209232e6e547",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 713,
                "h": 25,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 74,
                "y": 83
            }
        },
        {
            "Key": 728,
            "Value": {
                "id": "74c9681e-7ce9-4691-ba05-23f64878747f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 728,
                "h": 25,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 63,
                "y": 83
            }
        },
        {
            "Key": 729,
            "Value": {
                "id": "3b26bf29-d449-47f5-aedf-67b1af2bdefd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 729,
                "h": 25,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 57,
                "y": 83
            }
        },
        {
            "Key": 730,
            "Value": {
                "id": "5f087c95-fbdb-4493-b27d-531f7a0fe076",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 730,
                "h": 25,
                "offset": 5,
                "shift": 14,
                "w": 5,
                "x": 50,
                "y": 83
            }
        },
        {
            "Key": 731,
            "Value": {
                "id": "11e0244c-d1d4-432e-97bb-cd86c519b8ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 731,
                "h": 25,
                "offset": 4,
                "shift": 14,
                "w": 5,
                "x": 43,
                "y": 83
            }
        },
        {
            "Key": 732,
            "Value": {
                "id": "39b0c60c-e5c5-4e33-be79-8c86abe9a664",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 732,
                "h": 25,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 32,
                "y": 83
            }
        },
        {
            "Key": 733,
            "Value": {
                "id": "5c8b7aeb-9ce2-4070-b2fc-d199e152759e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 733,
                "h": 25,
                "offset": 3,
                "shift": 14,
                "w": 11,
                "x": 19,
                "y": 83
            }
        },
        {
            "Key": 894,
            "Value": {
                "id": "8e71d37e-943a-4d52-9d4c-071e66458daf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 894,
                "h": 25,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 376,
                "y": 110
            }
        },
        {
            "Key": 900,
            "Value": {
                "id": "2b58a81d-9bb8-4295-8b8f-1cb408b307c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 900,
                "h": 25,
                "offset": 6,
                "shift": 14,
                "w": 3,
                "x": 201,
                "y": 245
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 900
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 18,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}